<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Models\Author;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class AuthorApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = Author::class;
        $this->select = ["id","name","about"];
        $this->applyCount = true;
        $this->applyOrder  = true;
    }

    protected function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();
        if ($request->input("name"))
            $query->where("name","like","%" . $request->input("name") . "%");
        return $query;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name" => "required|string|min:5|max:50",
            "about" => "nullable|string|min:15|max:1000"
        ]);
        $author = new Author([
            "name" => $request->input("name"),
            "about" => $request->input("about")
        ]);
        $author->save();
        return $this->sendResponse($author);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Author $author)
    {
        $request->validate([
            "name" => "required|string|min:5|max:50",
            "about" => "required|string|min:15|max:1000"
        ]);
        $author->update([
            "name" => $request->input("name"),
            "about" => $request->input("about")
        ]);
        return $this->sendResponse($author);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::query()->findOrFail($id);
        if ($author->books->count()){
            return $this->sendError('Can Not Delete Author Has A Books',403);
        }
        Author::destroy($id);
        return $this->sendResponse();
    }
}
