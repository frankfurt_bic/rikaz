<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Http\Requests\BookRequest;
use App\Http\Requests\StatisticsRequest;
use App\Jobs\VisitTracker;
use App\Models;
use App\Models\Agent;
use App\Models\AppliedRange;
use App\Models\Book;
use App\Models\BookChapter;
use App\Models\BookClass;
use App\Models\Category;

use App\Models\Country;
use App\Models\NumberingType;
use App\Models\NumberingTypeRange;
use App\Models\Visit;
use App\Traits\ContentStatistics;
use Faker\Provider\Uuid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Stevebauman\Location\Facades\Location;
use function Laminas\Diactoros\normalizeUploadedFiles;

class BookApiController extends BaseApiController
{
    use ContentStatistics;

    public function __construct()
    {
        $this->class = Book::class;
        $this->applyCount = true;
        $this->applyOrder = true;
        $this->select = [
            "id",
            "name",
            "summary",
            "image",
            "book_identity_number",
            "publish_state",
            "author_id",
            "publisher_id"
        ];
    }

    protected function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();

        if ($request->input("category_id")) {
            $query->whereHas("categories", function (Builder $query) use ($request) {
                $query->where("category_id", $request->input("category_id"));
            });
        }

        if ($request->input("name"))
            $query->where("name", "like", "%" . $request->input("name") . "%");

        if ($request->input("author_id"))
            $query->where("author_id", $request->input("author_id"));

        if ($request->input("publisher_id"))
            $query->where("publisher_id", $request->input("publisher_id"));

        if ($request->input("numbering_state") !== null) {
            if ((boolean)$request->input("numbering_state"))
                $query->where("book_identity_number", 'NOT LIKE', '\__%');
            else
                $query->where("book_identity_number", 'like', '\__%');
        }

        if ($request->input("publish_state") !== null) {
            if ($request->input("publish_state"))
                $query->where("publish_state", Book::PUBLISHED);
            else {
                $query->where("publish_state", Book::WAITING)
                    ->orWhere("publish_state", Book::ABORTED);
            }
        }

        $query
            ->with("author:id,name,about", "publisher:id,name", 'publishRequest');
        return $query;
    }

    protected function show(Book $book)
    {
        $book->load('publisher')->load('knowledgeUnits')
            ->load("language")->load("book_size")
            ->load("cover_type")->load("copy_type")
            ->load("BookClass")->load("BookSubClass")
            ->load('categories')->load("author")
            ->load('publishRequest:book_id,updated_at')
        ;

        $user = \request()->user();
        $agent = $user != null ? $user->agent : null;
        $country = null;
        $location = Location::get(\request()->ip());
        if ($agent == null && $location != null)
            $country = Country::where('country_code', $location->countryCode)->first();
        VisitTracker::dispatch($agent, $book, $country != null ? $country->id : null, $location != null ? $location->countryCode : null);
        $book
            ->loadCount('visits')
            ->loadCount('knowledgeUnits');
        return $this->sendResponse($book);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $validatedRequest = $request->validated();
        $validatedRequest["book_identity_number"] = "__" . \Ramsey\Uuid\Uuid::uuid1()->toString();
        try {
            DB::beginTransaction();
            $book = new Book($validatedRequest);

            foreach ($validatedRequest['category_ids'] as $category_id) {
                $categorycount = Category::where('parent_id', $category_id)->count();
                if ($categorycount > 0)
                    abort(422, 'could not add because parent category have categories');
            }


            if ($request->file("image") != null) {
                $path = Storage::disk('public')->put('books_covers', $request->file("image"));
                $book->image = "/storage/" . $path;
            }
            $book->save();
            $book->categories()->sync($request->category_ids);
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            app('log')->error("error during store book", ['exception' => json_encode($e)]);
            throw $e;
        }
        return $this->sendResponse($book->fresh());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\book $book
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, Book $book)
    {
        $validatedRequest = $request->validated();
        try {
            DB::beginTransaction();
//            $book->numbering_type_id = $validatedRequest['numbering_type_id'];
//            $book->book_class_id = $validatedRequest['book_class_id'];
//            $book->book_sub_class_id = $validatedRequest['book_sub_class_id'];

            foreach ($validatedRequest['category_ids'] as $category_id) {
                $categorycount = Category::where('parent_id', $category_id)->count();
                if ($categorycount > 0)
                    abort(422, 'could not add because parent category have categories');
            }

            $book->update($validatedRequest);
            if ($request->file("image") != null) {
                $path = Storage::disk('public')->put('books_covers', $request->file("image"));
                if ($book->image != null) {
                    Storage::disk('public')->delete(str_replace("/storage", "", $book->image));
                }
                $book->image = "/storage/" . $path;
            }

            $book->categories()->sync($request->category_ids);
            $book->save();
            DB::commit();
        } catch (\Throwable $e) {
            DB::rollback();
            app('log')->error("error during update book", ['exception' => json_encode($e)]);
            throw $e;
        }
        return $this->sendResponse($book->fresh());
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\book $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(book $book)
    {
        DB::beginTransaction();
        $relatedCategories = Models\BookCategory::query()->where('book_id', $book->id);
        $relatedCategories->delete();
        foreach ($book->chapters as $item) {
            $item->knowledgeUnits()->delete();
        }

        $book->chapters()->delete();
        $book->knowledgeUnits()->delete();
        $book->applied_ranges()->delete();
        $book->save();

        Book::destroy($book->id);

        DB::commit();
        return $this->sendResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getCategoryBooks($categoryId)
    {
        $main_category = Category::query()->findOrFail($categoryId);
        $categoryIds = $main_category->getDescendants()->pluck('id')->toArray();
        array_push($categoryIds, $main_category->id);

        $books = Book::whereHas('categories', function ($query) use ($categoryIds) {
            $query->whereIn('category_id', $categoryIds);
        })->get();


        //    $c = $category->getDescendants()->with('books');

        //$categoryIds = $category->getDescendants()->pluck('id')->toArray();
        // array_push($categoryIds, $categoryId);

        // $books = Book::query()->whereHas('category_id', $categoryIds)->get();

        return $this->sendResponse($books);
    }

    public function addBookCategories(Request $request, $bookId)
    {
        Validator::make($request->all(), [
            'categories_ids' => 'required|array|min:1',
            'categories_ids.*' => 'distinct|exists:categories,id'
        ])->validate();


        $book = Book::findOrFail($bookId);
        $book->categories()->sync($request->input('categories_ids'));
    }

    public function generateBookAppliedRange(Request $request, Book $book)
    {
        $numberingType = NumberingType::query()->findOrFail($request->numberingtypeid);
        $bookscope = NumberingTypeRange::query()->where('numbering_type_id', $request->numberingtypeid)->where('type', 'bookscope')->first()->digit_number;
        $regagent = NumberingTypeRange::query()->where('numbering_type_id', $request->numberingtypeid)->where('type', 'regagent')->first()->digit_number;


        Validator::make($request->all(), [
            'numberingtypeid' => 'required|numeric|exists:numbering_type,id',
            'bookscope' => 'required|string|min:' . $bookscope . '|max:' . $bookscope,
            'regagent' => 'required|string|min:' . $regagent . '|max:' . $regagent,
        ])->validate();

        //if(!str_contains($book->book_identity_number , "__"))
        //  abort(422,'number already generated');

        $book->numbering_type_id = $request->numberingtypeid;
        $book->save();

        $book->load('numbering_type.numbering_type_ranges');

        $ranges = $book->numbering_type->numbering_type_ranges()->get();

        $appliedRanges = [];
        try {
            DB::beginTransaction();
            foreach ($ranges as $range) {
                $appliedRange = new AppliedRange();
                $appliedRange->numbering_type_range_id = $range->id;
                $appliedRange->book_id = $book->id;

                if ($range->type == 'bookscope') {
                    $appliedRange->digit_number = $request->bookscope;
                } else if ($range->type == 'class') {
                    $appliedRange->digit_number = $book->BookClass->number . $book->BookSubClass->number;
                } else if ($range->type == 'kucount') {
                    if ($book->ku_count < 100) {
                        $appliedRange->digit_number = 2;
                    } else if ($book->ku_count < 1000) {
                        $appliedRange->digit_number = 3;
                    } else if ($book->ku_count < 10000) {
                        $appliedRange->digit_number = 4;
                    }
                } else if ($range->type == 'regagent') {
                    $appliedRange->digit_number = $request->regagent;
                } else if ($range->type == 'booknumber') {
                    $result = $this->getMaxBooknumber($appliedRanges[0]['digit_number'], $appliedRanges[1]['digit_number'], $appliedRanges[2]['digit_number'], $appliedRanges[3]['digit_number']);
                    if ($result[0]->booknumber != null)
                        $max = (int)$result[0]->booknumber;
                    else
                        $max = 0;

                    $appliedRange->digit_number = str_pad($max + 1, $range->digit_number, '0', STR_PAD_LEFT);
                } else {
                    $appliedRange->digit_number = '';
                }
                $appliedRanges[] = $appliedRange->toArray();
            }


            AppliedRange::query()->insert($appliedRanges);

            $book->book_identity_number = $this->getBookIdentityNumber($appliedRanges);
            $book->save();

            DB::commit();
        } catch
        (\Throwable $e) {
            DB::rollback();
            app('log')->error("error during store book", ['exception' => json_encode($e)]);
            throw $e;
        }

        return $this->sendResponse($book->book_identity_number);
    }

    private function getMaxBooknumber($bookscope, $class, $kucount, $regagent)
    {
        $result = DB::select("select max(booknumber) as booknumber  from
                    (
                    select book_id,
                         max(case when (numbering_type_range.type='bookscope') then appl.digit_number else NULL end) as 'bookscope',
                         max(case when (numbering_type_range.type='class') then appl.digit_number else NULL end) as 'class',
                         max(case when (numbering_type_range.type='kucount') then appl.digit_number else NULL end) as 'kucount',
                         max(case when (numbering_type_range.type='regagent') then appl.digit_number else NULL end) as 'regagent',
                         max(case when (numbering_type_range.type='booknumber') then appl.digit_number else NULL end) as 'booknumber'
                     FROM applied_range as appl inner join numbering_type_range on
                     appl.numbering_type_range_id = numbering_type_range.id
                     where numbering_type_range.numbering_type_id = 1
                     group by book_id
                    ) as sub
                    where
                    bookscope =  '$bookscope' and
                    class =  '$class' and
                    kucount =  '$kucount' and
                    regagent =  '$regagent'");

        return $result;
    }

    private function getBookIdentityNumber($appliedRanges)
    {
        $result = '';
        foreach ($appliedRanges as $appliedRange)
            $result .= $appliedRange['digit_number'];

        return $result;
    }

    public function getTree(Book $book)
    {
        $chapters = $book->chapters()->without("children")->selectRaw(
            "id,JSON_EXTRACT(name, '$.ar') as name,parent_id"
        )->get()->toArray();
        $newChapters = [];
        foreach ($chapters as $chapter) {
            $chapter["type"] = 'CHAPTER';
            $chapter = (object)$chapter;
            array_push($newChapters, $chapter);
        }
        $units = $book->knowledgeUnits()->selectRaw(
            "id,header as name,book_chapter_id as parent_id"
        )->get()->toArray();
        $newUnits = [];
        foreach ($units as $unit) {
            $unit["type"] = 'KNOWLEDGE_UNIT';
            $unit = (object)$unit;
            array_push($newUnits, $unit);
        }
        $all = array_merge($newChapters, $newUnits);
        return $this->sendResponse($this->createTree($all));
    }

    public function createTree($array, $parent_id = null)
    {
        $tree = [];
        foreach ($array as $item) {
            if ($item->parent_id == $parent_id) {
                if ($item->type == 'CHAPTER') {
                    $item->children = $this->createTree($array, $item->id);
                }
                array_push($tree, $item);
            }
        }
        return $tree;
    }

    public function stats(StatisticsRequest $request,Book $book)

    {
        $data = $request->validated();
        return $this->sendResponse($this->contentStatistics($book, $data['start_date'], $data['end_date']));
    }
    public function dailyStats(StatisticsRequest $request,Book $book)
    {
        $data = $request->validated();

        return $this->sendResponse($this->statsiticsByDay($book, $data['start_date'], $data['end_date']));
    }
    public function monthlyStats(StatisticsRequest $request,Book $book){
        $data=$request->validated();

        return $this->sendResponse($this->statisticsByMonth( $book,$data['start_date'],$data['end_date']));
    }
    public function yearlyStats(StatisticsRequest $request,Book $book){
        $data=$request->validated();

        return $this->sendResponse($this->statisticsByYear( $book,$data['start_date'],$data['end_date']));
    }

    public function nextBook(Request $request,Book $book)
    {
        $request->validate([
            "type" => ['in:author,category,publisher'],
            'author_id' => [Rule::requiredIf($request->input('type') == 'author')],
            'category_id' => [Rule::requiredIf($request->input('type') == 'category')],
            'publisher_id' => [Rule::requiredIf($request->input('type') == 'publisher')],
        ]);
        $query = Book::where('order','>',$book->order)
            ->orderBy('order');
        if ($request->input('type') == 'author')
        {
             $query->where('author_id',$request->input('author_id'));
        }
        elseif ($request->input('type') == 'category')
        {
            $category_id = $request->input('category_id');
             $query->whereHas('categories', function ($query) use ($category_id) {
                $query->where('id', $category_id);
            });

        }
        elseif ($request->input('type') == 'publisher')
        {
            $query->where('publisher_id',$request->input('publisher_id'));
        }
        return $this->sendResponse($query->first());
    }

    public function previousBook(Request $request,Book $book)
    {
        $request->validate([
            "type" => ['in:author,category,publisher'],
            'author_id' => [Rule::requiredIf($request->input('type') == 'author')],
            'category_id' => [Rule::requiredIf($request->input('type') == 'category')],
            'publisher_id' => [Rule::requiredIf($request->input('type') == 'publisher')],
        ]);
        $query = Book::where('order','<',$book->order)
            ->orderByDesc('order');
        if ($request->input('type') == 'author')
        {
            $query->where('author_id',$request->input('author_id'));
        }
        elseif ($request->input('type') == 'category')
        {
            $category_id = $request->input('category_id');
            $query->whereHas('categories', function ($query) use ($category_id) {
                $query->where('id', $category_id);
            });

        }
        elseif ($request->input('type') == 'publisher')
        {
            $query->where('publisher_id',$request->input('publisher_id'));
        }
        return $this->sendResponse($query->first());
    }
    public function getMostVisitedBook(Request $request){
        $limit = $request->input('limit',10);
        $offset = $request->input('offset',0);
        $books = Book::query()->withCount('visits')->orderByDesc('visits_count')
            ->skip($offset)->take($limit)->get();
        return $this->sendResponse($books);
    }
}
