<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\BookChapterStoreRequest;
use App\Http\Requests\BookChapterUpdateRequest;
use App\Models\Book;
use App\Models\BookChapter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BookChapterApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = BookChapter::class;
        $this->with = ['knowledgeUnits'];
    }
    public function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();
        $request->validate([
            "book_id" => "required",
            "parent_id" => "nullable|exists:book_chapters,id"
        ]);
        $book = Book::query()->findOrFail($request->input("book_id"));
        $parent_id = $request->input("parent_id");
        $query->whereNull("parent_id");
        $query->where("book_id",$book->id);
        if ($parent_id != null)
            $query->where("parent_id",$parent_id);
        if ($request->input("main"))
            $query->without("children");
        return $query;
    }
    public function store(BookChapterStoreRequest $request)
    {
        $chapter = new BookChapter();
        $chapter->name = $request->input("name");
        $chapter->book_id = $request->input("book_id");
        if ($request->input("parent_id"))
            $chapter->parent_id = $request->input("parent_id");
        $chapter->save();

        return $this->sendResponse($chapter->fresh());
    }
    public function update(BookChapterUpdateRequest $request, $id)
    {
        $chapter = BookChapter::query()->find($id);
        $chapter->name = $request->input("name");
        $chapter->parent_id = $request->input("parent_id");
        $chapter->save();

        return $this->sendResponse($chapter->fresh());
    }
    public function destroy($id)
    {
        $chapter = BookChapter::query()->find($id);
        if (count($chapter->children))
            return $this->sendError('لا يمكن حذف فصل يحتوي فصول فرعية',401);
        if (count($chapter->knowledgeUnits))
            return $this->sendError('لا يمكن حذف فصل يحتوي وحدات معرفية',401);

        BookChapter::destroy($id);
        return $this->sendResponse();
    }


}
