<?php

namespace App\Http\Controllers\ApiControllers;

use App\Models\BookPublishRequest;
use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BookPublishRequestApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = BookPublishRequest::class;
        $this->applyCount = true;
        $this->applyOrder  = true;
    }

    protected function beforeIndex(Builder $query)
    {
        $query->where("state",BookPublishRequest::WAITING);
        return $query;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "book_id" => "required|exists:books,id|unique:book_publish_requests"
        ]);
        $r = BookPublishRequest::create([
            "book_id" => $request->input("book_id")
        ]);
        $r->fresh()->book()->update([
            "publish_state" => BookPublishRequest::WAITING
        ]);
        return  $this->sendResponse($r->fresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        if (!auth('api')->user()->is_admin)
//            return $this->sendError('Permission Denied',403);
        $request->validate([
            "approved" => "required|boolean"
        ]);
        $book = Book::query()->findOrFail($id);
        $book->publish_state = (boolean) $request->input("approved") ? Book::PUBLISHED : Book::ABORTED;
        $book->save();
        $publish_request = $book->publishRequest;
        $publish_request->state = (boolean) $request->input("approved") ? BookPublishRequest::PUBLISHED : BookPublishRequest::ABORTED;
        $publish_request->save();
        return $this->sendResponse($book->fresh());
    }

}
