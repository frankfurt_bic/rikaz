<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\KnowledgeUnitStoreRequest;
use App\Http\Requests\KnowledgeUnitUpdateRequest;
use App\Http\Requests\StatisticsRequest;
use App\Imports\KuImport;
use App\Jobs\VisitTracker;
use App\Models\Agent;
use App\Models\Book;
use App\Models\BookChapter;
use App\Models\Country;
use App\Models\Gender;
use App\Models\KnowledgeUnit;
use App\Models\KuAccessory;
use App\Models\KuImage;
use App\Models\Visit;
use App\Traits\ContentStatistics;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use OwenIt\Auditing\Models\Audit;
use Stevebauman\Location\Facades\Location;

class KnowledgeUnitApiController extends BaseApiController
{
    Use ContentStatistics;
    public function __construct()
    {
        $this->class = KnowledgeUnit::class;
        $this->with = ["accessories.type"];
    }

    protected function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();
        $request->validate([
            "book_id" => ["exists:books,id",Rule::requiredIf(!$request->chapter_id)],
            "chapter_id" => ["exists:book_chapters,id",Rule::requiredIf(!$request->book_id)]
        ]);

        $book_id = $request->input("book_id");
        $chapter_id = $request->input("chapter_id");
        if ($book_id){
            $query->where("book_id",$book_id);
            if ($request->input("base"))
                $query->whereDoesntHave("bookChapter");
        }
        if ($chapter_id)
            $query->where("book_chapter_id",$chapter_id);
//        if (!auth("api")->user()->is_admin)
//            $query->where("publish_state",1);
        $query->select("id", "number","full_number", "header", "summary","book_id","book_chapter_id","publish_state");
        return $query;
    }

    protected function beforeShow(Builder $query)
    {
        $query->with("book")->with("bookChapter")->with('publishRequest')->withCount('visits');
        return $query;
    }
    protected function show(KnowledgeUnit $knowledgeUnit)
    {
        $user=\request()->user();
        $agent = $user!=null?$user->agent:null;
        $location=Location::get(\request()->ip());
        $country=null;
        if ($agent==null&&$location!=null)
            $country= Country::where('country_code',$location->countryCode)->first();
        VisitTracker::dispatch($agent,$knowledgeUnit,$country!=null?$country->id:null,$location!=null?$location->countryCode:null);
        return $this->sendResponse($knowledgeUnit);
    }

    public function store(KnowledgeUnitStoreRequest $request)
    {
        request()->query->set('identity_string', Str::orderedUuid());
        $data = $request->validated();
        $data["book_id"] = $data["book_id"] ?? BookChapter::query()->find($data["book_chapter_id"])->book_id;

        $number = 0;

        $book = Book::findOrFail($data["book_id"]);
        if ($book->book_identity_number == null || str_starts_with($book->book_identity_number,"__") ){
            return $this->sendError("Add Knowledge Units Denied,Request Book Numbering Before",403);
        }
        if (str_contains($book->book_identity_number , "__")) {
            $this->sendResponse("book identity number is not generated");
        }

        $range = $book->numbering_type->numbering_type_ranges()->where('type', 'kunumber')->first();

        $maxNumber = (int)KnowledgeUnit::where('book_id', $data["book_id"])->max('number');

        if ($maxNumber != null)
            $number = $maxNumber;

        $data["number"] = str_pad($number + 1, $range->digit_number, '0', STR_PAD_LEFT);
        $data["full_number"] = $book->book_identity_number . $data["number"];

        $unit = KnowledgeUnit::create($data);

        if (isset($data["accessories"]) && count($data["accessories"])){
            foreach ($data["accessories"] as $item) {
                $item = json_decode($item);
                $unit->accessories()->create([
                    "type_id" => $item->type_id,
                    "text" => $item->text
                ]);
            }
        }

        return $this->sendResponse($unit->fresh());
    }

    public function update(KnowledgeUnitUpdateRequest $request, $id)
    {
        request()->query->set('identity_string', Str::orderedUuid());
        $data = $request->validated();
        $unit = KnowledgeUnit::query()->findOrFail($id);
        $editTrack = $unit->editsTrack()->create($unit->fresh()->toArray());
        $unit->update($data);
        $current_accessories = $unit->accessories;
        if (isset($data["accessories"]) && count($data["accessories"])){
            $coming_accessories = [];
            foreach ($data["accessories"] as $item) {
                $item = json_decode($item);
                if (isset($item->id)){
                    $coming_accessories[] = $item;
                    $current = KuAccessory::query()->find($item->id);
                    if ($current->text != $item->text || $current->type_id != $item->type_id){
                        $editTrack->accessoriesEditsTrack()->create([
                            "type_id" => $item->type_id,
                            "text" => $item->text
                        ]);
                        KuAccessory::find($item->id)->update([
                            "type_id" => $item->type_id,
                            "text" => $item->text
                        ]);
                    }
                }
                else{
                    $unit->accessories()->create([
                        "type_id" => $item->type_id,
                        "text" => $item->text
                    ]);
                }
            }
            foreach ($current_accessories  as $item){
                if (!collect($coming_accessories)->pluck("id")->contains($item->id)){
                    $editTrack->accessoriesEditsTrack()->create([
                        "type_id" => $item->type_id,
                        "text" => $item->text
                    ]);
                    KuAccessory::find($item->id)->destroy($item->id);
                }
            }
        }
        else{
            foreach ($current_accessories  as $item){
                    $editTrack->accessoriesEditsTrack()->create([
                        "type_id" => $item->type_id,
                        "text" => $item->text
                    ]);
                    KuAccessory::find($item->id)->destroy($item->id);
            }
        }

        return $this->sendResponse($unit->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = KnowledgeUnit::query()->findOrFail($id);
        DB::beginTransaction();
        $unit->accessories()->delete();
        $unit->delete();
        DB::commit();
        return $this->sendResponse();
    }

    public function storeFromExcel(Request $request)
    {
        $request->validate([
            "excel" => "required|mimes:xls,xlsx"
        ]);
        $file = $request->file("excel");
        Excel::import(new KuImport(), $file);
    }

    public function storeImage(Request $request)
    {
        $request->validate([
            "image" => "required"
        ]);
        $path = Storage::disk("public")->put("ku_images", $request->file("image"));
        $path = "/storage/" . $path;
        $image = new KuImage();
        $image->image = $path;
        $image->save();
        return $this->sendResponse($path);
    }

    public function getEditTrack(KnowledgeUnit $knowledgeUnit){
        $knowledgeUnit = $knowledgeUnit->with(['editsTrack.accessoriesEditsTrack'])->first();
        return $this->sendResponse($knowledgeUnit);
    }

    public function getAudit(KnowledgeUnit $knowledgeUnit)//todo find better way
    {
        $accessories = Audit::query()->where('auditable_type' , KuAccessory::class)//get relevant accessory audits
            ->whereJsonContains('old_values->knowledge_unit_id',$knowledgeUnit->id)
            ->orWhereJsonContains('new_values->knowledge_unit_id',$knowledgeUnit->id)
            ->get();
        $knowledge_units=$knowledgeUnit->audits()->get();                                       //get relevent ku audits
        foreach ($knowledge_units as $knowledge_unit){                                          //assign each ku audit its relevant accessory audit
            $kuAccessories = $accessories->where('tags' , $knowledge_unit->tags)
                ->sort(function ($prev,$next){return $prev->id-$next->id;});
            $knowledge_unit->accessories = $kuAccessories;
            $accessories=$accessories->diff($kuAccessories);
        }
        foreach ($accessories as $accessory)                                                     //group remaining accessory audits by their tags and assign each group a virtual ku audit
        {
            $newKuAccessories = $accessories->where('tags' , $accessory->tags);
            if($newKuAccessories->count()>0) {
                $ku = new Audit(['id' => 0, 'event' => 'updated',
                    'auditable_type' => KnowledgeUnit::class, 'auditable_id' => 0, 'created_at' => $newKuAccessories->first()->created_at]);
                $ku->accessories = $newKuAccessories;
                $knowledge_units->add($ku);
                $accessories = $accessories->diff($newKuAccessories);
            }
        }

        return $this->sendResponse($knowledge_units->sort(function ($ku1,$ku2){return $ku1->created_at<>$ku2->created_at;}));
    }
    public function stats(StatisticsRequest $request,KnowledgeUnit $knowledgeUnit){
        $data=$request->validated();
        return $this->sendResponse($this->contentStatistics($knowledgeUnit,$data['start_date'],$data['end_date']));
    }
    public function dailyStats(StatisticsRequest $request,KnowledgeUnit $knowledgeUnit){
        $data=$request->validated();

        return $this->sendResponse($this->statsiticsByDay($knowledgeUnit,$data['start_date'],$data['end_date']));
    }
    public function monthlyStats(StatisticsRequest $request,KnowledgeUnit $knowledgeUnit){
        $data=$request->validated();

        return $this->sendResponse($this->statisticsByMonth($knowledgeUnit,$data['start_date'],$data['end_date']));
    }
    public function yearlyStats(StatisticsRequest $request,KnowledgeUnit $knowledgeUnit){
        $data=$request->validated();

        return $this->sendResponse($this->statisticsByYear($knowledgeUnit,$data['start_date'],$data['end_date']));
    }
}
