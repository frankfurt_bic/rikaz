<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use App\KnowledgeUnitReport;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class KnowledgeUnitReportApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = KnowledgeUnitReport::class;
        $this->applyCount = true;
        $this->applyOrder = true;
        $this->with = [
            'knowledgeUnit',
            'agent',
            'user'
        ];
    }

    protected function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();
        $request->validate([
            "status" => ['nullable','in:approved,waiting,aborted'],
            'knowledge_unit_id' => ['nullable','exists:knowledge_units,id']
        ]);
        $user = auth('api')->user();
        if (!$user->is_admin && !$user->agent){
            return  $this->sendError('You Do Not Have A Permissions To Show That',403);
        }
        if ($user->agent){
            $query->where('agent_id',$user->agent->id);
        }
        if ($request->input('status')){
            switch ($request->input('status')){
                case 'approved':
                    $query->where('status',KnowledgeUnitReport::APPROVED);
                    break;
                case 'waiting':
                    $query->where('status',KnowledgeUnitReport::WAITING);
                    break;
                case 'aborted':
                    $query->where('status',KnowledgeUnitReport::ABORTED);
                    break;
            }

        }
        if ($request->input("search")){
            $query->where('report_text','like','%' . $request->input('search') . '%')
                ->orWhere('reply_text','like','%' . $request->input('search') . '%')
                ->orWhereHas('knowledgeUnit',function (Builder $q) use ($request){
                    $q->where('header','like','%' . $request->input('search') . '%');
                });
        }
        if ($request->input('knowledge_unit_id')){
            $query->where('knowledge_unit_id',$request->input('knowledge_unit_id'));
        }
        return $query->with('knowledgeUnit:id,header');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth('api')->user();
        if (!$user->agent){
            return $this->sendError('Permission Denied',403);
        }
        $request->validate([
            "knowledge_unit_id" => ['required','exists:knowledge_units,id'],
            "report_text" => ['required','min: 50','max:1000'],
        ]);
        $report = new KnowledgeUnitReport();
        $report->knowledge_unit_id = $request->input("knowledge_unit_id");
        $report->report_text = $request->input("report_text");
        $report->agent_id = $user->agent->id;
        $report->save();
        return $this->sendResponse($report->fresh());
    }

    protected function beforeShow(Builder $query)
    {
        $user = auth('api')->user();
        if(!$user->is_admin){
            $query->where("agent_id",$user->agent->id ?? null);
        }
        return $query;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth('api')->user();
        if (!$user->is_admin){
            return  $this->sendError('You Do Not Have A Permissions To Show That',403);
        }
        $request->validate([
            "approved" => ['required','boolean'],
            'reply_text' => ['nullable','string']
        ]);
        $report = KnowledgeUnitReport::query()->findOrFail($id);
        $report->user_id = $user->id;
        $report->status = (boolean) $request->input("approved") ? KnowledgeUnitReport::APPROVED : KnowledgeUnitReport::ABORTED;
        if ($request->input("reply_text"))
            $report->reply_text = $request->input("reply_text");
        $report->save();
        return $this->sendResponse($report->fresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
