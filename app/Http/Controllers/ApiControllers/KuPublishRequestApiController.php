<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\ApiControllers\BaseApiController;
use App\KuPublishRequest;
use App\Models\BookPublishRequest;
use App\Models\KnowledgeUnit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Book;


class KuPublishRequestApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = KuPublishRequest::class;
        $this->applyCount = true;
        $this->applyOrder  = true;
    }

    protected function beforeIndex(Builder $query)
    {
        $query->where("state",KuPublishRequest::WAITING);
        return $query;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "knowledge_unit_id" => "required|exists:knowledge_units,id|unique:ku_publish_requests"
        ]);
        $kuPublishRequest = KuPublishRequest::create([
            "knowledge_unit_id" => $request->input("knowledge_unit_id")
        ]);
        $kuPublishRequest->fresh()->knowledgeUnit()->update([
            "publish_state" => KuPublishRequest::WAITING
        ]);
        return  $this->sendResponse($kuPublishRequest->fresh());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KuPublishRequest  $kuPublishRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            "approved" => "required|boolean"
        ]);
        $knowledgeUnit = KnowledgeUnit::query()->findOrFail($id);
        $knowledgeUnit->publish_state = (boolean) $request->input("approved") ? Book::PUBLISHED : Book::ABORTED;
        $knowledgeUnit->save();
        $publish_request = $knowledgeUnit->publishRequest;
        $publish_request->state = (boolean) $request->input("approved") ? KuPublishRequest::PUBLISHED : KuPublishRequest::ABORTED;
        $publish_request->save();
        return $this->sendResponse($knowledgeUnit->fresh());
    }

}
