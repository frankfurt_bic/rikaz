<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Models\BookClass;
use App\Models\BookSize;
use App\Models\BookSubClass;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use App\Models\NumberingType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\KuAccessoryType;
class LookUpApiController extends BaseApiController
{
    public function getCopyTypes(Request $request)
    {
        $value = CopyType::get();
        return $this->sendResponse($value);
    }

    public function getCoverTypes(Request $request)
    {
        $value = CoverType::get();
        return $this->sendResponse($value);
    }

    public function getLanguages(Request $request)
    {
        $value = Language::get();
        return $this->sendResponse($value);
    }

    public function getBookSizes(Request $request)
    {
        $value = BookSize::get();
        return $this->sendResponse($value);
    }
    public function getImage($path){
        $file = Storage::disk('public')->download($path);
        if (!$file)
            return abort(404);
        return $file;
    }

    public function getBookClass(Request $request)
    {
        $value = BookClass::get();
        return $this->sendResponse($value);
    }

    public function getBookSubClass($class_id)
    {
        $value = BookSubClass::query()->where('book_class_id',$class_id)->get();
        return $this->sendResponse($value);
    }

    public function getNumberingTypes(Request $request)
    {
        $value = NumberingType::query()->with('numbering_type_ranges')->get();
        return $this->sendResponse($value);
    }

    public function getKuAccessoriesType(Request $request)
    {
        $value = KuAccessoryType::query()->get();
        return $this->sendResponse($value);
    }
}
