<?php

namespace App\Http\Controllers\ApiControllers\admin;

use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Models\Publisher;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use phpseclib\Crypt\Hash;

class AdminApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = User::class;
        $this->applyCount = true;
        $this->applyOrder = true;
    }

    protected function beforeIndex(Builder $query)
    {
        $request = $this->getRequest();
        $query->where('is_admin',true);
        $search = $request->input("search");
        if ($search){
            $query->where(function (Builder $q) use ($search) {
                $q->where("name","like","%$search%")
                    ->orWhere("email","like","%$search%");
            });
        }
        if ($request->input("activation_state") != -1 && $request->input("activation_state") !== null){
            $query->where("is_active",(boolean) $request->input("activation_state"));
        }
        return $query;
    }

    protected function beforeShow(Builder $query)
    {
        return $query->where("is_admin",true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $admin)
    {
        $request->validate([
            "name" => ['required','string','min:5','max:30'],
            'email' => ['required','email'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed', "case_diff", "numbers", "letters", "symbols"],

        ]);

        if ($request->input("password"))
            $admin->password = \Illuminate\Support\Facades\Hash::make($request->input("password"));
        if ($request->file("image")){
            if ($admin->image != null)
                \Storage::disk('public')->delete($admin->image);
            $admin->image = \Storage::disk('public')->put('admin/images',$request->file('image'));
        }
        $admin->name = $request->input("name");
        $admin->email = $request->input('email');
        $admin->save();
        return $this->sendResponse($admin);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publisher $publisher)
    {
        //
    }
}
