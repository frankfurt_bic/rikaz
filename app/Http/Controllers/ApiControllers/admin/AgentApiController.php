<?php

namespace App\Http\Controllers\ApiControllers\admin;

use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Models\Publisher;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use phpseclib\Crypt\Hash;

class AgentApiController extends BaseApiController
{
    public function __construct()
    {
        $this->class = User::class;
        $this->applyCount = true;
        $this->applyOrder = true;
    }

    protected function beforeIndex(Builder $query)
    {

        $request = $this->getRequest();
        $query->whereHas('agent');
        $search = $request->input("search");
        if ($search){
            $query->where(function (Builder $q) use ($search){
                $q->where("name","like","%$search%")
                    ->orWhere("email","like","%$search%")
                    ->orWhereHas('agent',function (Builder $q) use ($search){
                        $q->where("name","like","%$search%");
                    });
            });
        }
        if ($request->input("activation_state") != -1 && $request->input("activation_state") !== null){
            $query->where("is_active",(boolean) $request->input("activation_state"));
        }
        return $query;
    }

    protected function beforeShow(Builder $query)
    {
        return $query->whereHas('agent');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, User $agent)
//    {
//        $request->validate([
//            "name" => ['required','string','min:5','max:30'],
//            'email' => ['required','email'],
//            'password' => ['confirmed','nullable']
//        ]);
//        $agent->name = $request->input("name");
//        if ($request->input("password"))
//            $agent->password = \Illuminate\Support\Facades\Hash::make($request->input("password"));
//        $agent->publisher->name = $request->input("name");
//        $agent->publisher->save();
//        $agent->email = $request->input('email');
//        $agent->save();
//        return $this->sendResponse($agent);
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Publisher  $publisher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Publisher $publisher)
    {
        //
    }
}
