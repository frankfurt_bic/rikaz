<?php

namespace App\Http\Controllers\Auth;

use App\Models\Agent;

use App\Models\Country;
use App\Models\MobileToken;
use App\Models\User;
use App\Traits\ApiDecorator;
use App\Traits\VisitorStatistics;
use Kreait\Laravel\Firebase\Facades\FirebaseMessaging;
use Propaganistas\LaravelPhone\PhoneNumber;


class AgentApiController extends BaseUserApiController
{
    use VisitorStatistics;
    public function __construct()
    {
        $this->phone_required = false;
    }


    protected function afterRegisteringUser(User $user, $data)
    {
        $agent = new Agent();
        $agent->user_id = $user->id;
        $agent->first_name = $data["first_name"];
        $agent->last_name = $data["last_name"];
        $agent->save();
        $user->save();
    }

    /**
     * @param $user_id
     * @return User
     */
    protected function getUserWithRelation($user_id)
    {
        $user = User::where('id', $user_id)
            ->with("agent")
            ->first();
        return $user;
    }

    protected function addRoleToRegister()
    {
        return [
            "first_name" => "required|string|min:3|max:20",
            "last_name" => "required|string|min:3|max:20"
        ];
    }

    function subscribeTokens(string $token_type, User $user, $token)
    {
        $topic = "agent";
        FirebaseMessaging::subscribeToTopic($topic, $token);
    }


    function unsubscribeToken($token,$user)
    {
        FirebaseMessaging::unsubscribeFromTopic("agent", $token);
    }

    public function  stats(Agent $agent)
    {
        return $this->sendResponse($this->visitorStatistics($agent));
    }

}
