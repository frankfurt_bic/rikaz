<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\MobileToken;
use App\Models\PhoneVerificationCode;
use App\Notifications\VerifyEmailNotification;
use App\Traits\ApiDecorator;
use App\Models\User;
use Carbon\Carbon;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Facades\Socialite;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;
use Illuminate\Support\Str;

abstract class BaseUserApiController extends Controller
{
    use ApiDecorator;

    protected $social_client_secret;
    protected $social_client_id;
    protected $phone_required = true;
    protected $login_after_register = true;

    protected $email_required = true;
    private $firstTimeIntervalToSendNewVerificationSMS = 10;
    private $intervalToSendNewVerificationSMS = 60 * 5;

    /**
     * @param Request $request
     * @return JsonResponse|mixed
     * @throws ValidationException
     */
    public function registerByEmail(Request $request)
    {
        $user = $this->register($request, false);
        if ($user) {
            $this->sendEmailVerificationCode($user);
            // login if requested by guest, otherwise only create account
            if ($this->login_after_register && $request->user() == null)
                return $this->loginByEmail($request);
        }
        return $this->sendError('can\'t create account', 411);
    }

    /**
     * @param Request $request
     * @return JsonResponse|mixed
     * @throws ValidationException
     */
    public function registerByPhone(Request $request)
    {
        $user = $this->register($request, true);
        if ($user) {
            $this->sendSMSVerificationCode($user);
            // login if requested by guest, otherwise only create account
            if ($this->login_after_register && $request->user() == null)
                return $this->loginByPhone($request);
        }
        return $this->sendError('can\'t create account', 411);
    }

    /**
     * @param Request $request
     * @param boolean $by_phone
     * @return JsonResponse|mixed
     * @throws ValidationException
     */
    private function register(Request $request, $by_phone)
    {
        $data = $this->validationToRegister($request, $by_phone);
        $data['password'] = $this->getPasswordHash($data["password"]);
        $data["name"] = $request->post("name") != null ? $request->post("name") : $request->post("first_name") . " " . $request->post("last_name");
        $data["verification_code"] = Str::random(20);

        return DB::transaction(function () use ($data) {
            $user = User::create($data);
            if ($user) {
                $this->afterRegisteringUser($user, $data);
            }
            return $user;
        });

    }

    /**
     * @param Request $request
     * @param boolean $by_phone
     * @return array
     * @throws ValidationException
     */
    private function validationToRegister(Request $request, $by_phone)
    {
        $rules = [
            'client_id' => 'required',
            'client_secret' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed', "case_diff", "numbers", "letters", "symbols"],
        ];
        if ($by_phone) {
            $rules ['phone'] = ['required', 'phone:AUTO,mobile', 'unique:users'];
            $rules ['email'] = [$this->email_required ? 'required' : 'nullable', 'string', 'email', 'max:255', 'unique:users'];
        } else {
            $rules ['email'] = ['required', 'string', 'email', 'max:255', 'unique:users'];
            $rules ['phone'] = [$this->phone_required ? 'required' : 'nullable', 'phone:AUTO,mobile', 'unique:users'];
        }
        return Validator::make($request->all(), array_merge($rules, $this->addRoleToRegister()))->validate();
    }


    /**
     * @return array
     */
    protected function addRoleToRegisterValidation()
    {
        return [];
    }

    /**
     * @param string $password
     * @return string
     */
    private function getPasswordHash($password)
    {
        return Hash::make($password);
    }

    /**
     * @param User $user
     * @param array $data
     */
    protected abstract function afterRegisteringUser(User $user, $data);


    /**
     * @param string $token_type
     * @param User $user
     * @param $token
     */
    abstract function subscribeTokens(string $token_type, User $user, $token);

    /**
     * @param $token
     */
    abstract function unsubscribeToken($token, User $user);

    /**
     * @param User $user
     * @return bool
     */
    protected function sendSMSVerificationCode(User $user)
    {
        try {
            $smsProvider = env("VerificationCodeProvider", "twilio");
            if ($smsProvider == "twilio") {
                $client = new Client(config('twilio.sid'), config('twilio.auth_token'));
                $service = $client->verify->v2->services(config('twilio.verification_sid'));
                $verification = $service->verifications->create($user->phone, 'sms', [
                    'locale' => 'ar']);
                PhoneVerificationCode::create([
                    "phone" => $user->phone,
                    "verification_sid" => $verification->sid]);
                if ($verification->status == "denied") {
                    Log::debug("verification status is denied in sendSMSVerificationCode" . json_encode($verification));
                    return false;
                }
                return true;
            } else if ($smsProvider == "turkeysms") {
                $code = rand(1111, 9999);
                $api_key = config('turkeysms.api_key');                            //ضع المفتاح الخاص بك الموجود بحسابك هنا
                $title = config('turkeysms.title');                            //ضع عنوان الارسال هنا .. يجب ان يكون فعال في حسابك
                $text = config('turkeysms.message') . $code;                            //محتوى الرسالة النصية هنا
                $sentto = $user->phone;                            //رقم الموبايل المستلم للرسالة
                $body = array("api_key" => $api_key, "title" => $title, "text" => $text, "sentto" => $sentto);
                $json = json_encode($body);
                $ch = curl_init('https://www.turkeysms.com.tr/api/v2/gonder/add-content');
                $header = array('Content-Type: application/json');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                $result = curl_exec($ch);
                curl_close($ch);
                PhoneVerificationCode::create([
                    "phone" => $user->phone,
                    "code" => $code]);
                return true;
            } elseif ($smsProvider == "jawalsms") {
                $code = rand(1111, 9999);
                $client = new GuzzleClient();
                $params = [
                    'query' => [
                        // 'outputFormat' => 'json',
                        'username' => config('jawalsms.username'),
                        'password' => config('jawalsms.password'),
                        'sender' => config('jawalsms.sender'),
                        'mobile' => $user->phone,
                        'message' => config('jawalsms.message') . $code,
                        'unicode' => config('jawalsms.unicode'),
                    ]
                ];
                $uri = 'http://www.jawalsms.net/httpSmsProvider.aspx';
                $result = $client->request('Get', $uri, $params)->getBody()->getContents();
                $result = substr($result, 0, strpos($result, "\r\n"));
                if ($result == "0") {
                    PhoneVerificationCode::create([
                        "phone" => $user->phone,
                        "code" => $code]);
                    return true;
                }
                Log::warning("sendSMSVerificationCode error code: " . $result);
                return false;
            } elseif ($smsProvider == "test") {
                $code = "123456";
                PhoneVerificationCode::create([
                    "phone" => $user->phone,
                    "code" => $code]);
                return true;
            }
        } catch (TwilioException $e) {
            Log::error("sendSMSVerificationCode TwilioException" . $e->getMessage());
            return false;
        } catch (\Exception $exception) {
            Log::error("sendSMSVerificationCode Exception" . $exception->getMessage());
            return false;
        }
        return false;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function reSendSMSVerificationCode(Request $request)
    {
        $interval = $this->intervalToSendNewVerificationSMS($request->user());
        if ($interval > 0) {
            return $this->sendError("you must try after {$interval} seconds", 411, $interval);
        }

        return $this->sendSMSVerificationCode($request->user()) == true ? $this->sendResponse() :
            $this->sendError("Failed Send SMS Verification Code", 411);
    }

    /**
     * @param Request $request
     * @return void
     */
    public function canSendSMSVerificationCode(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        $interval = $this->intervalToSendNewVerificationSMS($user);
        if ($interval != 0) {
            return $this->sendError("you must try after {$interval} seconds", 411, $interval);
        }
        return $this->sendResponse();
    }

    /**
     * @param User $user
     * @return  int
     */
    private function intervalToSendNewVerificationSMS(User $user)
    {
        $phone = $user->phone;
        $res = PhoneVerificationCode::where("phone", $phone)
            ->orderByDesc("created_at")->get("created_at");

        if ($res->count() == 0)
            return 0;

        $subSeconds = $res->count() == 1 ? $this->firstTimeIntervalToSendNewVerificationSMS : $this->intervalToSendNewVerificationSMS * ($res->count() - 1);

        $allowedTime = Carbon::now()->subSeconds($subSeconds);
        $lastTime = Carbon::make($res->first()["created_at"]);
        if ($allowedTime > $lastTime)
            return 0;
        return $allowedTime->diffInSeconds($lastTime);
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function reSendMailVerificationCode(Request $request)
    {
        return $this->sendEmailVerificationCode($request->user()) == true ? $this->sendResponse() :
            $this->sendError("Failed Send Mail Verification Code", 411);
    }

    /**
     * @param User $user
     * @return bool
     */
    protected function sendEmailVerificationCode(User $user)
    {
        try {
            $user->sendEmailVerificationNotification();
            return true;
        } catch (\Exception $exception) {
            Log::error("sendEmailVerificationCode" . $exception->getMessage());
            return false;
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse|mixed
     * @throws ValidationException
     */
    public function loginByPhone(Request $request)
    {
        return $this->loginApi($request, true);
    }

    /**
     * @param Request $request
     * @return JsonResponse|mixed
     * @throws ValidationException
     */
    public function loginByEmail(Request $request)
    {
        return $this->loginApi($request, false);
    }

    /**
     * @param Request $request
     * @param $by_phone
     * @return JsonResponse|mixed
     * @throws ValidationException
     */
    private function loginApi(Request $request, $by_phone)
    {
        $this->validationToLogin($request, $by_phone);
        $r = Request::create('/oauth/token', 'POST',
            [
                'grant_type' => 'password',
                'client_id' => $request->input('client_id'),
                'client_secret' => $request->input('client_secret'),
                'scope' => '*',
                'username' => $by_phone ? $request->input('phone') : $request->input('email'),
                'password' => $request->input('password'),
            ]);
        /** @var Response $response */
        $response = app()->handle($r);
        return $this->processPassportLoginResponse($response, $by_phone ? $request->input('phone') : $request->input('email'), $by_phone);
    }


    /**
     * @param Request $request
     * @param $by_phone
     * @return array
     * @throws ValidationException
     */
    private function validationToLogin(Request $request, $by_phone)
    {
        $rule = [
            'password' => ['required', 'string'],
            'client_id' => 'required',
            'client_secret' => 'required',
        ];

        if ($by_phone)
            $rule ['phone'] = ['required', 'phone:AUTO,mobile'];
        else
            $rule ['email'] = ['required', 'string', 'email', 'max:255'];

        return Validator::make($request->all(), $rule)->validate();
    }

    /**
     * @param Response $response
     * @param $by_target
     * @param $by_phone
     * @return JsonResponse|mixed
     */
    private function processPassportLoginResponse(Response $response, $by_target, $by_phone)
    {
        if ($response->status() == 200) {
            $res = json_decode($response->content(), true);
            $user_id = User::where($by_phone ? 'phone' : 'email', $by_target)->firstOrFail()->id;
            $res['user'] = $this->getUserWithRelation($user_id);
            return $this->sendResponse($res);
        } else if ($response->status() == 400 || $response->status() == 401) {

            return response()->json("معلومات تسجيل الدخول غير صحيحة", 400);
        }
        return response()->json($response->content(), $response->status());
    }

    /**
     * @param $user_id
     * @return User
     */
    protected function getUserWithRelation($user_id)
    {
        return User::findOrFail($user_id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function socialLogin(Request $request, $provider)
    {
        return Socialite::driver($provider)
            ->stateless()
            ->redirect()
            ->getTargetUrl();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function callBack_socialLogin(Request $request, $provider)
    {
        $accessToken = $request->input('access_token');
        if ($accessToken) {
            /** @var \Laravel\Socialite\Two\User $providerUser */
            $providerUser = Socialite::driver($provider)->userFromToken($accessToken);
        } else {
            $providerUser = Socialite::driver($provider)->stateless()->user();
        }
        if ($providerUser) {
            $r = Request::create('/oauth/token', 'POST',
                [
                    'grant_type' => 'social',
                    'client_id' => $this->social_client_id,
                    'client_secret' => $this->social_client_secret,
                    'scope' => '*',
                    'provider' => $provider,
                    'access_token' => $providerUser->token,
                ]);
            /** @var Response $response */
            $response = app()->handle($r);
            return $this->processPassportLoginResponse($response, $providerUser->getEmail(), false);
        }
        return $this->sendError("failed", 411);
    }


    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function refreshToken(Request $request)
    {
        $rule = [
            'refresh_token' => ['required'],
            'client_id' => 'required',
            'client_secret' => 'required',
        ];

        Validator::make($request->all(), $rule)->validate();
        $r = Request::create('/oauth/token', 'POST',
            [
                'grant_type' => 'refresh_token',
                'refresh_token' => $request->input('refresh_token'),
                'client_id' => $request->input('client_id'),
                'client_secret' => $request->input('client_secret'),
                'scope' => '',
            ]);

        return app()->handle($r);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function saveInstanceId(Request $request)
    {
        Validator::validate($request->all(), ["token" => "required"]);
        /** @var User $user */
        $user = $request->user();
        $token = $request->input("token");
        // in case of error
        MobileToken::where('token', $token)->delete();
        $token_type = $request->input("token_type");
        if (!in_array($token_type, [MobileToken::TYPE_APNS_IPHONE, MobileToken::TYPE_FIREBASE_ANDROID, MobileToken::TYPE_FIREBASE_WEB, MobileToken::TYPE_FIREBASE_IPHONE])) {
            $token_type = MobileToken::TYPE_UNKNOWN;
        }
        $mobile_token = new MobileToken();
        $mobile_token->token = $token;
        $mobile_token->type = $token_type;
        $user->mobileTokens()->save($mobile_token);
        $this->subscribeTokens($token_type, $user, $token);

        return $this->sendResponse();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function deleteInstanceId(Request $request)
    {
        Validator::validate($request->all(), ["token" => "required"]);
        $token = $request->input("token");
        MobileToken::where('token', $token)->delete();
        $user = $request->user();
        $this->unsubscribeToken($token, $user);
        return $this->sendResponse();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function verifyPhone(Request $request)
    {
        Validator::validate($request->all(), ["code" => "required"]);
        /** @var User $user */
        $user = $request->user();
        $res = $this->verificationSMSCode($request->input("code"), $user);
        if ($res) {
            $user->phone_verified_at = Carbon::now();
            $user->save();
            return $this->sendResponse();
        } else return
            $this->sendError("verify Phone failed", 411);
    }

    /**
     * @param $code
     * @param User $user
     * @return bool
     */
    private function verificationSMSCode($code, User $user)
    {
        try {
            $smsProvider = env("VerificationCodeProvider", "twilio");
            if ($smsProvider == "twilio") {
                $client = new Client(config('twilio.sid'), config('twilio.auth_token'));
                $service = $client->verify->v2->services(config('twilio.verification_sid'));
                $verification = $service->verificationChecks->create($code,
                    ['to' => phone($user->phone)->formatE164()]);
                if ($verification->status == 'approved') {
                    PhoneVerificationCode::where("phone", $user->phone)->delete();
                    return true;
                }
                Log::debug("verification status is denied in verificationSMSCode" . json_encode($verification));
                return false;
            } elseif ($smsProvider == "jawalsms" || $smsProvider == "turkeysms" || $smsProvider == "test") {
                $phone = PhoneVerificationCode::where("phone", $user->phone)->orderByDesc("created_at")->firstOrFail();
                if ($phone->code == $code) {
                    $phone->delete();
                    return true;
                }
            }
        } catch (TwilioException $e) {
            Log::error("sendSMSVerificationCode TwilioException" . $e->getMessage());
            return false;
        }
        return false;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function verifyEmail(Request $request)
    {
        if (!$request->hasValidSignature()) {
            return redirect(Config::get('app.clientUrl'). "notVerified");
        }

        $user = User::findOrFail($request->input("id"));

        if (sha1($user->email) != $request->input("hash")) {
            return redirect(Config::get('app.clientUrl'). "notVerified");
        }

        if ($user->hasVerifiedEmail()) {
            return redirect(Config::get('app.clientUrl'));
        }

        $user->markEmailAsVerified();
        $user->save();
        return redirect(Config::get('app.clientUrl') . "verified");
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function profile(Request $request)
    {
        return $this->sendResponse($this->getUserWithRelation($request->user()->id));
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function saveLocale(Request $request)
    {
        $rules = [
            'locale' => 'required|string',
        ];
        Validator::make($request->all(), $rules)->validate();
        /** @var User $user */
        $user = $request->user();
        $input = $request->input('locale', 'ar');
        if ($input != "en")
            $input = 'ar';
        $user->locale = $input;
        $user->save();
        return $this->sendResponse();
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function forgetByEmail(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];
        Validator::make($request->all(), $rules)->validate();

        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResponse(Password::RESET_LINK_SENT)
            : $this->sendError($response, 411);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function forgetByPhone(Request $request)
    {
        $rules = [
            'phone' => ['required', 'phone:AUTO,mobile', 'exists:users,phone'],
        ];
        $date = Validator::make($request->all(), $rules)->validate();

        /** @var User $user */
        $user = User::where('phone', $date['phone'])->first();

        if (is_null($user)) {
            return $this->sendError(Password::INVALID_USER, 411);
        }

        $interval = $this->intervalToSendNewVerificationSMS($user);
        if ($interval > 0) {
            return $this->sendError("you must try after {$interval} seconds", 411, $interval);
        }

        $res = $this->sendSMSVerificationCode($user);

        return $res == true
            ? $this->sendResponse(Password::RESET_LINK_SENT)
            : $this->sendError("Error in send SMS .. see logs", 411);
    }

    /**
     * @param Request $request
     * @return bool
     * @throws ValidationException
     */
    public function resetByEmail(Request $request)
    {
        $data = $this->validationToResetByEmail($request);

        $response = Password::broker()->reset(
            $data,
            function (User $user, $password) {
                $user->password = Hash::make($password);
                $user->save();
                event(new PasswordReset($user));
            }
        );

        return $response == Password::PASSWORD_RESET
            ? $this->loginApi($request, false)
            : $this->sendError($response, 411);
    }

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    private function validationToResetByEmail(Request $request)
    {
        $rules = [
            'client_id' => 'required',
            'client_secret' => 'required',
            'token' => 'required',
            'email' => 'required|email',
            'password' => ['required', 'string', 'min:8', 'confirmed', "case_diff", "numbers", "letters", "symbols"],
        ];
        return Validator::validate($request->all(), $rules);
    }

    /**
     * @param Request $request
     * @return bool
     * @throws ValidationException
     */
    public function resetByPhone(Request $request)
    {
        $data = $this->validationToResetByPhone($request);

        $user = User::where('phone', $data['phone'])->first();

        /** @var User $user */
        if ($user == null)
            return $this->sendError(Password::INVALID_USER, 411);

        $state = $this->verificationSMSCode($data['code'], $user);

        if ($state) {
            $user->password = $this->getPasswordHash($data["password"]);
            $user->phone_verified_at = Carbon::now();
            $user->save();
            return $this->loginApi($request, true);
        }

        return $this->sendError("failed", 411);

    }

    /**
     * @param Request $request
     * @return array
     * @throws ValidationException
     */
    private function validationToResetByPhone(Request $request)
    {
        $rules = [
            'client_id' => 'required',
            'client_secret' => 'required',
            'code' => 'required',
            'phone' => 'required|phone:AUTO,mobile',
            'password' => ['required', 'string', 'min:5', 'confirmed'],
        ];
        return Validator::validate($request->all(), $rules);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function update(Request $request)
    {
        $rules = [
            'name' => ['nullable', 'string', 'max:255'],
            'email' => ['nullable', 'string', 'email', 'max:255', 'unique:users,email,' . $request->user()->id],
            'phone' => ['nullable', 'phone:AUTO,mobile', 'unique:users,phone,' . $request->user()->id],
            'password' => ['nullable', 'string', 'min:8', "confirmed"],
            'Uimage' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:1000000'],
        ];

        Validator::make($request->all(), $rules)->validate();

        /** @var User $user */
        $user = $request->user();
        if ($request->input("email") != null) {
            $user->email = $request->input("email");
        }
        if ($request->input("phone") != null) {
            $user->phone = $request->input("phone");
            $user->phone_verified_at = null;
        }
        if ($request->input("name") != null)
            $user->name = $request->input("name");
        if ($request->input("password") != null)
            $user->password = Hash::make($request->input("password"));

        if ($request->file("Uimage") != null) {
            $path = Storage::disk('public')->put('user_images', $request->file("Uimage"));
            if ($user->image != null)
                Storage::disk('public')->delete($user->image);
            $user->image = $path;
        }
        $user->save();
        return $this->sendResponse($this->getUserWithRelation($user->id));
    }


}
