<?php

namespace App\Http\Controllers\Auth;

use App\Models\Agent;

use App\Models\Country;
use App\Models\MobileToken;
use App\Models\Publisher;
use App\Models\User;
use App\Traits\ApiDecorator;
use Kreait\Laravel\Firebase\Facades\FirebaseMessaging;
use Propaganistas\LaravelPhone\PhoneNumber;


class PublisherApiController extends BaseUserApiController
{
    public function __construct()
    {
        $this->phone_required = false;
        $this->login_after_register = false;

    }


    protected function afterRegisteringUser(User $user, $data)
    {
        $publisher = new Publisher();
        $publisher->user_id = $user->id;
        $publisher->name = $data["name"];
        $publisher->save();
        $user->save();
    }

    /**
     * @param $user_id
     * @return User
     */
    protected function getUserWithRelation($user_id)
    {
        $user = User::where('id', $user_id)
            ->with("publisher")
            ->first();
        return $user;
    }

    protected function addRoleToRegister()
    {
        return [
            "name" => "required|string|min:5|max:30",
        ];
    }

    function subscribeTokens(string $token_type, User $user, $token)
    {
        $topic = "publisher";
        FirebaseMessaging::subscribeToTopic($topic, $token);
    }


    function unsubscribeToken($token,$user)
    {
        FirebaseMessaging::unsubscribeFromTopic("publisher", $token);
    }


}
