<?php

namespace App\Http\Controllers\Auth;

use App\Models\Agent;

use App\Models\Country;
use App\Models\MobileToken;
use App\Models\User;
use App\Traits\ApiDecorator;
use Illuminate\Http\Request;
use Kreait\Laravel\Firebase\Facades\FirebaseMessaging;
use Propaganistas\LaravelPhone\PhoneNumber;


class UserApiController extends BaseUserApiController
{
    public function __construct()
    {
        $this->phone_required = false;
    }


    protected function afterRegisteringUser(User $user, $data)
    {

    }

    /**
     * @param $user_id
     * @return User
     */
    protected function getUserWithRelation($user_id)
    {
        $user = User::where('id', $user_id)->first();
        return $user;
    }

    protected function addRoleToRegister()
    {
        return [
            "first_name" => "required|string|min:3|max:20",
            "last_name" => "required|string|min:3|max:20"
        ];
    }

    function subscribeTokens(string $token_type, User $user, $token)
    {
        if ($user->publisher != null){
            $topic = "publisher";
        }
        else if ($user->agent != null){
            $topic = "agent";  ;
        }
        else{
            $topic = "admin";
        }

        FirebaseMessaging::subscribeToTopic($topic, $token);
    }


    function unsubscribeToken($token,User $user)
    {
        if ($user->publisher != null){
            $topic = "publisher";
        }
        else if ($user->agent != null){
            $topic = "agent";  ;
        }
        else{
            $topic = "admin";
        }
        FirebaseMessaging::unsubscribeFromTopic($topic, $token);
    }
    public function getUser(){
        return $this->sendResponse(["user" => auth('api')->user()]);
    }
    public function toggleActive(User $user){
        $user->is_active = !$user->is_active;
        $user->save();
        return $this->sendResponse($user->fresh());
    }

}
