<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Redirect;

class EnsureEmailIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $redirectToRoute
     */
    public function handle($request, Closure $next)
    {
        if (! $request->user() ||
            ! $request->user()->hasVerifiedEmail()) {
            return abort(413, 'Your email address is not verified.');
        }

        return $next($request);
    }
}
