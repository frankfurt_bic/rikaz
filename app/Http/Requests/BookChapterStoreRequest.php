<?php

namespace App\Http\Requests;

use App\Models\BookChapter;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookChapterStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            "name" => "required|string|min:5|max:100",
            "book_id" => "required|exists:books,id",
            "parent_id" => ["nullable",Rule::exists('book_chapters','id')->where('book_id',$this->book_id)],
        ];
        return $rules;
    }
}
