<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'order' => 'nullable|numeric',
            'author_id' => 'required|exists:authors,id',

            'numbering_type_id' => 'exists:numbering_type,id',

            'ku_count' => 'required|numeric',

            "book_class_id"=>'required|exists:book_class,id',
            "book_sub_class_id"=> 'required|exists:book_sub_class,id',

            'publisher_id' => 'required|exists:publishers,id',
            'edition_number' => 'required|numeric',
            'language_id' => 'required|exists:languages,id',
            'pages_count' => 'nullable|numeric',
            'folders_count' => 'nullable|numeric',
            'isbn' => 'nullable|string|min:13|max:13',
            'ikun' => 'nullable|numeric',
            'cover_type_id' => 'nullable|exists:cover_types,id',
            'book_size_id' => 'nullable|exists:book_sizes,id',
            'publication_year' => 'nullable|numeric',
            'copy_type_id' => 'nullable|exists:copy_types,id',
            'image' => 'nullable',
            'summary' => 'nullable|string',
            'category_ids' => 'array|required',
            'category_ids.*' => 'exists:categories,id',
        ];
    }

}
