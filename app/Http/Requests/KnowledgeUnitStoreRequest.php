<?php

namespace App\Http\Requests;

use App\Rules\JsonArray;
use App\Rules\JsonArrayUniqueTypeId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class KnowledgeUnitStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "header" => "required|string|min:5|max:200",
            "summary" => "string|min:0|max:100",
            "body" => "required|string|min:100|max:1000000",
            "book_id" => ["exists:books,id",Rule::requiredIf(!$this->book_chapter_id)],
            "book_chapter_id" => ["exists:book_chapters,id",Rule::requiredIf(!$this->book_id)],
            "accessories" => ['nullable',"array",new JsonArrayUniqueTypeId()],
            "accessories.*" => ["json",new JsonArray()],
        ];
    }
}
