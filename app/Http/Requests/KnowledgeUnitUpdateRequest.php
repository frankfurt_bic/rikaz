<?php

namespace App\Http\Requests;

use App\Models\BookChapter;
use App\Models\KnowledgeUnit;
use App\Rules\JsonArray;
use App\Rules\JsonArrayUniqueTypeId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class KnowledgeUnitUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "header" => "required|string|min:5|max:200",
            "summary" => "nullable|string|min:0|max:100",
            "body" => "required|string|min:100|max:1000000",
            "accessories" => ["nullable","array", new JsonArrayUniqueTypeId()],
            "accessories.*" => ["json",new JsonArray()]
        ];
    }
}
