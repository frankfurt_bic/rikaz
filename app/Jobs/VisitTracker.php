<?php

namespace App\Jobs;

use App\Models\Visit;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use mysql_xdevapi\Exception;

class VisitTracker implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $visitor_id;
    private $visitor_type;
    private $country_id;
    private $content_type;
    private $content_id;
    private $gender_id;
private $countryCode;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($visitor, $content, $country_id, $countryCode)
    {
        $this->visitor_id=$visitor!=null?$visitor->id:null;
        $this->visitor_type=$visitor!=null?get_class($visitor):null;
        $this->country_id=$visitor!=null?$visitor->country_id:$country_id;
        $this->content_type=$content!=null?get_class($content):null;
        $this->content_id=$content->id;
        $this->gender_id=$visitor!=null?$visitor->gender_id:null;
        $this->countryCode = $countryCode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Visit::create( [
            'visitor_id' => $this->visitor_id,
            'visitor_type' => $this->visitor_type,
            'content_id' => $this->content_id,
            'content_type' => $this->content_type,
            'country_id' => $this->country_id,
            'gender_id' => $this->gender_id,
            'country_code' => $this->countryCode
        ]);


    }
}
