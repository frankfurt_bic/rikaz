<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KUEditTrack extends Model
{
    protected $fillable = [
        'header',
        'summary',
        'body',
        'knowledge_unit_id'
    ];
    public function accessoriesEditsTrack(){
        return $this->hasMany(KuAccessoriesEditTrack::class);
    }
}
