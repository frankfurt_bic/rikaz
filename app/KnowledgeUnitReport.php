<?php

namespace App;

use App\Models\Agent;
use App\Models\KnowledgeUnit;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class KnowledgeUnitReport extends Model
{
    const WAITING = 0;
    const APPROVED = 1;
    const ABORTED = 2;


    public function knowledgeUnit(){
        return $this->belongsTo(KnowledgeUnit::class);
    }
    public function agent(){
        return $this->belongsTo(Agent::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
