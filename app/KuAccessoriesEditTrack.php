<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KuAccessoriesEditTrack extends Model
{
    protected $fillable = [
        "text",
        "type_id",
        "k_u_edit_track_id"
    ];
}
