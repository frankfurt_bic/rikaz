<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    //
    protected $table='agents';
    public function visits()
    {
        return $this->morphMany(Visit::class, 'visitor');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

}
