<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppliedRange extends Model
{
    protected $table = 'applied_range';
    protected $fillable = [
        "numbering_type_range_id",
        "book_id",
        "digit_number",
    ];

    public function numbering_type_range()
    {
        return $this->belongsTo(NumberingTypeRange::class, "numbering_type_range_id");
    }

    public function book()
    {
        return $this->belongsTo(book::class, "book_id");
    }

}
