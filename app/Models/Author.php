<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Author extends Model
{
    use HasTranslations;
    protected $table = 'authors';
    public $translatable = ['name','about'];
    protected $fillable = [
      "name",
      "about"
    ];
    protected $appends = [
        "is_deletable"
    ];

    public function books(){
        return $this->hasMany(Book::class);
    }

    public function getIsDeletableAttribute(){
        $books = Book::query()->where("author_id",$this->id)->count();
        $state = true;
        if ($books > 0)
            $state = false;
        return $state;
    }
}
