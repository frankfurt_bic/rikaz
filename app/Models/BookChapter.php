<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class BookChapter extends Model
{
    use HasTranslations;
    public $translatable = ['name'];
    protected $appends = [
        "max_depth",
        "parent_name",
        "book_name",
    ];
    protected $with = [
        "children"
    ];
    public function getMaxDepthAttribute(){
        return CategoryClosure::query()->where("ancestor",$this->id)->max("depth");
    }
    public function children()
    {
        return $this->hasMany(BookChapter::class,'parent_id');
    }
    public function parent()
    {
        return $this->belongsTo(BookChapter::class, 'parent_id');
    }
    public function book()
    {
        return $this->belongsTo(Book::class, 'book_id');
    }
    public function knowledgeUnits()
    {
        return $this->hasMany(KnowledgeUnit::class);
    }
    public function getParentNameAttribute()
    {
        $parent = null;
        if ($this->parent_id)
            $parent = BookChapter::query()->where("id",$this->parent_id)->first()->name;
        return $parent;
    }
    public function getBookNameAttribute()
    {
        $name = $this->book_id ?  Book::query()->find($this->book_id)->name : null;
        return $name;
    }
}
