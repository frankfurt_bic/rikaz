<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
* @mixin \Eloquent
 */
class BookClass extends Model
{
    protected $table = 'book_class';
    protected $fillable = ["number","name"];

    public function BookSubClass()
    {
        return $this->hasMany(BookSubClass::class, 'book_class_id');
    }
}
