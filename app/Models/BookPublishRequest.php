<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BookPublishRequest
 *
 * @property int $id
 * @property int $book_id
 * @property int $state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Book $book
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookPublishRequest whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookPublishRequest extends Model
{
    const PRE_PRODUCTION = 0;
    const WAITING = 1;
    const PUBLISHED = 2;
    const ABORTED = 3;

    protected $fillable = [
        "book_id"
    ];
    public function book(){
        return $this->belongsTo(Book::class);
    }
}
