<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\BookSize
 *
 * @property int $id
 * @property array $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookSize newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookSize newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookSize query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookSize whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookSize whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookSize whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BookSize whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BookSize extends Model
{
    use HasTranslations;
    public $translatable = ['name'];
    protected $table = 'book_sizes';
    protected $fillable = ["name"];
}
