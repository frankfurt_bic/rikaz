<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookSubClass extends Model
{
    protected $table = 'book_sub_class';
    protected $fillable = ["number","name","book_class_id"];

    public function book_class()
    {
        return $this->belongsTo(BookClass::class, "book_class_id");
    }
}
