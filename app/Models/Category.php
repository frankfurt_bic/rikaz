<?php

namespace App\Models;

use Franzose\ClosureTable\Models\Entity;
use App\Models\CategoryClosure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Translatable\HasTranslations;


/**
 * App\Models\Category
 *
 * @property int $id
 * @property int $parent_id
 * @property int $position
 * @property array $name
 * @property string|null $notes
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Book[] $books
 * @property-read int|null $books_count
 * @property-read \Franzose\ClosureTable\Extensions\Collection|\App\Models\Category[] $children
 * @property-read int|null $children_count
 * @property-read mixed $max_depth
 * @property-read array $translations
 * @property-read \App\Models\Category|null $parent
 * @method static \Franzose\ClosureTable\Extensions\Collection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity ancestors()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity ancestorsOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity ancestorsWithSelf()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity ancestorsWithSelfOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity childAt($position)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity childNode()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity childNodeOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity childOf($id, $position)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity childrenRange($from, $to = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity childrenRangeOf($id, $from, $to = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity descendants()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity descendantsOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity descendantsWithSelf()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity descendantsWithSelfOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity firstChild()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity firstChildOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity firstSibling()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity firstSiblingOf($id)
 * @method static \Franzose\ClosureTable\Extensions\Collection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity lastChild()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity lastChildOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity lastSibling()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity lastSiblingOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity neighbors()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity neighborsOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity nextSibling()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity nextSiblingOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity nextSiblings()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity nextSiblingsOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity prevSibling()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity prevSiblingOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity prevSiblings()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity prevSiblingsOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity sibling()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity siblingAt($position)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity siblingOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity siblingOfAt($id, $position)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity siblings()
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity siblingsOf($id)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity siblingsRange($from, $to = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Franzose\ClosureTable\Models\Entity siblingsRangeOf($id, $from, $to = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Category extends Entity
{
    use HasTranslations;

    protected $table = 'categories';

    public $translatable = ['name'];

    protected $fillable = [
        "name",
        "parent_id",
        "position",
        "notes"
    ];
    protected $with = [
        "children"
    ];
    protected $appends = [
        "max_depth",
        "is_deletable",
        "parent_name",
        "books_count"
    ];

    /**
     * ClosureTable model instance.
     *
     * @var \App\Models\CategoryClosure
     */
    protected $closure = 'App\Models\CategoryClosure';

    public function children()
    {
        return $this->hasMany(Category::class,'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }
    public function getParentNameAttribute()
    {
        $parent = null;
        if ($this->parent_id)
            $parent = Category::query()->where("id",$this->parent_id)->first()->name;
        return $parent;
    }

    public function getMaxDepthAttribute(){
        return CategoryClosure::query()->where("ancestor",$this->id)->max("depth");
    }

    public function books()
    {
        return $this->belongsToMany(Book::class, 'book_category', 'category_id','book_id' );
    }
    public function getIsDeletableAttribute(){
        $books = $this->books->count();
        $children =  $this->children()->count();
        $state = true;
        if ($books > 0 || $children > 0)
            $state = false;
        return $state;
    }
    public function getBooksCountAttribute(){
        return $this->books()->count();
    }

}
