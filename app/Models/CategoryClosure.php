<?php
namespace App\Models;

use Franzose\ClosureTable\Models\ClosureTable;

/**
 * App\Models\CategoryClosure
 *
 * @property int $closure_id
 * @property int $ancestor
 * @property int $descendant
 * @property int $depth
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryClosure newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryClosure newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryClosure query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryClosure whereAncestor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryClosure whereClosureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryClosure whereDepth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CategoryClosure whereDescendant($value)
 * @mixin \Eloquent
 */
class CategoryClosure extends ClosureTable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category_closure';
}
