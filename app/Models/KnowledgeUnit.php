<?php

namespace App\Models;

use App\KnowledgeUnitReport;
use App\KUEditTrack;
use App\KuPublishRequest;
use App\Models\Book;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * App\Models\KnowledgeUnit
 *
 * @property int $id
 * @property string $number
 * @property string $header
 * @property string $body
 * @property int $book_id
 * @property int $publish_state
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KuAccessory[] $accessories
 * @property-read int|null $accessories_count
 * @property-read \App\Models\Book $book
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereBookId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit wherePublishState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KnowledgeUnit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class KnowledgeUnit extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = "knowledge_units";
    protected $fillable = ["header","summary","body","book_id","number","full_number","book_chapter_id","publish_state"];
    protected $with = ["accessories"];
    protected $appends = [
        "next_ku",
        "previous_ku",
        "extra_details"
    ];
    public function generateTags(): array
    {
        $req=request();
        if ($req!=null){
            $id = request()->query->get('identity_string');
            if ($id != null)
                return [
                    $id
                ];
        }
        return [];
    }
    public function accessories(){
        return $this->hasMany(KuAccessory::class);
    }
    public function book(){
        return $this->belongsTo(Book::class);
    }
    public function bookChapter(){
        return $this->belongsTo(BookChapter::class);
    }
    public function editsTrack(){
        return $this->hasMany(KUEditTrack::class);
    }

    public function getNextKuAttribute(){
        return KnowledgeUnit::query()->where("book_id",$this->book_id)->where('id', '>', $this->id)
            ->select('id','book_chapter_id')->first();
    }

    public function getPreviousKuAttribute(){
       return  KnowledgeUnit::query()->where("book_id",$this->book_id)->where('id', '<', $this->id)
           ->select('id','book_chapter_id')->orderByDesc('id')->first();
    }

    public function getExtraDetailsAttribute(){
        $extra = new \stdClass();
        if ($this->book_id){
            $book = Book::query()->find($this->book_id);
            $author = $book->author;
            $chapter = BookChapter::query()->find($this->book_chapter_id);
            $extra->publisher_name = $book->publisher->name;
            $extra->book_name = $book->name;
            $extra->book_categories = $book->categories()->pluck("name")->toArray();
            $extra->book_publication_year = $book->publication_year;
            $extra->book_edition_number = $book->edition_number;
            $extra->folders_count = $book->folders_count;
            $extra->book_language = $book->language->name;
            $extra->author_name = $author->name;
            $extra->chapter_name = $chapter ? $chapter->name : null;
        }

        return $extra;
    }
    public function visits()
    {
        return $this->morphMany(Visit::class, 'content');
    }
    public function reports(){
        return $this->hasMany(KnowledgeUnitReport::class);
    }

    public function publishRequest()
    {
        return $this->hasOne(KuPublishRequest::class);
    }
}
