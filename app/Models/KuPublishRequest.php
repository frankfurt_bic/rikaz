<?php

namespace App;


use App\Models\KnowledgeUnit;
use Illuminate\Database\Eloquent\Model;

class KuPublishRequest extends Model
{
    const PRE_PRODUCTION = 0;
    const WAITING = 1;
    const PUBLISHED = 2;
    const ABORTED = 3;

    protected $table='ku_publish_requests';

    protected $fillable = [
        "knowledge_unit_id"
    ];
    protected $with = ["knowledgeUnit"];
    public function knowledgeUnit(){
        return $this->belongsTo(KnowledgeUnit::class);
    }
}
