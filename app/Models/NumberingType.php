<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumberingType extends Model
{
    protected $table = 'numbering_type';
    protected $fillable = ["name"];

    public function books()
    {
        return $this->hasMany(book::class,'numbering_type_id');
    }


    public function numbering_type_ranges()
    {
        return $this->hasMany(NumberingTypeRange::class,'numbering_type_id');
    }
}
