<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumberingTypeRange extends Model
{

    protected $table = 'numbering_type_range';
    protected $fillable =
           ["digit_number",
            "numbering_type_id",
            "order",
            "type"];

    public function applied_ranges()
    {
        return $this->hasMany(AppliedRange::class, 'numbering_type_range_id');
    }

    public function numbering_type()
    {
        return $this->belongsTo(NumberingType::class, "numbering_type_id");
    }
}
