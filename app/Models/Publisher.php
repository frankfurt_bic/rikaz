<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

/**
 * App\Models\Publisher
 *
 * @property int $id
 * @property array $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read array $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Publisher extends Model
{
    use HasTranslations;
    public $translatable = ['name'];
    public $appends = ['books_count','published_books_count'];

    protected $table = 'publishers';
    protected $fillable = ["name"];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function books(){
        return $this->hasMany(Book::class);
    }
    public function getBooksCountAttribute(){
        return $this->books()->count();
    }
    public function getPublishedBooksCountAttribute(){
        return $this->books()->where("publish_state",Book::PUBLISHED)->count();
    }
}
