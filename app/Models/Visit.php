<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $table='visits';
    protected $fillable = [
      'visitor_type','visitor_id','content_type','content_id','country_id','gender_id','country_code'
    ];
    public function visitor()
    {
        return $this->morphTo();

    }

    public function content()
    {
        return $this->morphTo();

    }
}
