<?php

namespace App\Rules;

use App\Models\KnowledgeUnit;
use Illuminate\Contracts\Validation\Rule;

class JsonArray implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    protected $msg;
    public function passes($attribute, $value)
    {
        $state = true;
        if (!isset(json_decode($value)->text)){
            $this->msg =  'text is required';
            $state = false;
        }
        if (!isset(json_decode($value)->type_id)){
            $this->msg =  'type_id is required';
            $state = false;
        }
        return  $state;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->msg;
    }
}
