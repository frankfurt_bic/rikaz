<?php

namespace App\Rules;

use App\Models\KnowledgeUnit;
use Illuminate\Contracts\Validation\Rule;

class JsonArrayUniqueTypeId implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    protected $defarray;
    public function passes($attribute, $value)
    {
        $state = true;
        $type_ids = [];
        foreach ($value as $item){
            $item = json_decode($item);
            array_push($type_ids,$item->type_id);
        }
        $state = count($value) == count(array_unique($type_ids));
        $this->defarray = array();
        $filterarray = array();
        foreach($type_ids as $val){
            if (isset($defarray[$val])) {
                $filterarray[] = $val;
            }
            $this->defarray[$val] = $val;
        }
        return  $state;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "duplicated type id " . implode(",",$this->defarray);
    }
}
