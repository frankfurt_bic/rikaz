<?php

namespace App\Traits;

use App\Models\Visit;

trait ContentStatistics
{

    public function contentStatistics($content,$start_date,$end_date){
        $query=$content->visits()->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date);
        $stats=$this->statistics(with(clone $query));
        $all_visits=$stats['visits'];
        //$all_visitors=$stats['visitors'];
        $countries_rate =$all_visits?  $query->groupBy('country_id')
            ->selectRaw("round(100*count(*)/$all_visits,2) as rate, country_id")
            ->get():[];
//        $genders_rate =  $content->visits()->groupBy('gender_id')todo fix this query and uncomment
//            ->selectRaw("round(100*count(visitor_id)/$all_visitors,2) as rate, gender_id")
//            ->get();
        return (array_merge($stats,[
            'countries_rate'=> $countries_rate,
            //'genders_rate'=> $genders_rate,
        ]));
    }
    private function statistics($query) {
        $all_visits=(int)$query->count();
        $anonymous_users=with(clone $query)->whereNull('visitor_id')->count();
        $registered_users=with(clone $query)->distinct()->count(['visitor_id','visitor_type']);
        $all_visitors= $registered_users+$anonymous_users;
        return ([
            'visits'=> $all_visits,
            'visitors'=> $all_visitors,
            'registered_users'=>$all_visitors?round(100*$registered_users/$all_visitors,2):0,
            'anonymous_users' =>$all_visitors?round(100*$anonymous_users/$all_visitors,2):0
        ]);
    }
    public function statsiticsByDay($content,$start_date,$end_date){
        $query=$content->visits()->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date);
        $stats=$this->statistics(with(clone $query));
        $all_visits=$stats['visits'];
        $days=$all_visits?$query->groupBy('year','month','day')
            ->selectRaw("count(*) as visits ,round(100*count(*)/$all_visits,2) as visit_rate, YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day")//todo add visitors and the rest of report items
            ->get():[];
        return array_merge($stats,[
            'days'=>$days
        ]);
    }

    public function statisticsByMonth($content,$start_date,$end_date){
        $query=$content->visits()->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date);
        $stats=$this->statistics(with(clone $query));
        $all_visits=$stats['visits'];
        $months=$all_visits?$query->groupBy('year','month')
            ->selectRaw("count(*) as visits ,round(100*count(*)/$all_visits,2) as visit_rate, YEAR(created_at) year, MONTH(created_at) month")//todo add visitors and the rest of report items
            ->get():[];
        return array_merge($stats,['months'=>$months]);
    }
    public function statisticsByYear($content,$start_date,$end_date){
        $query=$content->visits()->whereDate('created_at','>=',$start_date)->whereDate('created_at','<=',$end_date);
        $stats=$this->statistics(with(clone $query));
        $all_visits=$stats['visits'];
        $years=$all_visits?$query->groupBy('year')
            ->selectRaw("count(*) as visits ,round(100*count(*)/$all_visits,2) as visit_rate, YEAR(created_at) year")//todo add visitors and the rest of report items
            ->get():[];
        return array_merge($stats,['years'=>$years]);
    }

}
