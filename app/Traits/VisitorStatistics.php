<?php

namespace App\Traits;

use App\Models\Book;
use App\Models\KnowledgeUnit;

trait VisitorStatistics
{

    public function visitorStatistics($visitor){
        $all_visits=(int)$visitor->visits()->count();
        $all_items = (int)$visitor->visits()->distinct()->count(['content_id','content_type']);
        $all_books=$visitor->visits()->where('content_type',Book::class)->distinct()->count(['content_id','content_type']);
        $all_books_visits=(int)$visitor->visits()->where('content_type',Book::class)->count();
        $all_knowledge_units=$visitor->visits()->where('content_type',KnowledgeUnit::class)->distinct()->count(['content_id','content_type']);
        $all_knowledge_units_visits=(int)$visitor->visits()->where('content_type',KnowledgeUnit::class)->count();
        $books_rate =  $visitor->visits()->where('content_type',Book::class)
            ->groupBy('content_id')->selectRaw("round(100*count(*)/$all_books_visits,2) as rate, content_id")->get();
        $knowledge_units_rate =  $visitor->visits()->where('content_type',KnowledgeUnit::class)
            ->groupBy('content_id')->selectRaw("round(100*count(*)/$all_knowledge_units_visits,2) as rate, content_id")->get();
        $book_class_rate =  $visitor->visits()->where('content_type',Book::class)
            ->Join('books', function($q) {
                $q->on('content_id', '=', 'books.id');
                $q->where('content_type', '=', Book::class);
            })->groupBy('books.book_class_id')->selectRaw("round(100*count(*)/$all_books_visits,2) as rate, books.book_class_id")->get();
        $book_sub_class_rate =  $visitor->visits()->where('content_type',Book::class)
            ->Join('books', function($q) {
                $q->on('content_id', '=', 'books.id');
                $q->where('content_type', '=', Book::class);
            })->groupBy('books.book_sub_class_id')->selectRaw("round(100*count(*)/$all_books_visits,2) as rate, books.book_sub_class_id")->get();
        return ([
            'all_visits'=> $all_visits,
            'all_items'=> $all_items,
            'all_books'=> $all_books,
            'all_books_visits'=> $all_books_visits,
            'all_knowledge_units'=> $all_knowledge_units,
            'all_knowledge_units_visits' => $all_knowledge_units_visits,
            'books_rate' => $books_rate,
            'knowledge_units_rate' => $knowledge_units_rate,
            'book_class_rate' => $book_class_rate,
            'book_sub_class_rate' => $book_sub_class_rate,
        ]);
    }

}
