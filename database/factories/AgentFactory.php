<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Gender;
use App\Models\Agent;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Agent::class, function (Faker $faker) {
    $first_name = $faker->name;
    $last_name = $faker->name;
    return [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'user_id' => factory(User::class)->create([
            "name" => $first_name . " " . $last_name
        ])->id,
        "gender_id" => (Gender::query()->first()??factory(Gender::class)->create())->id,
        "country_id" => (\App\Models\Country::query()->first()??factory(\App\Models\Country::class)->create())->id,
    ];
});
