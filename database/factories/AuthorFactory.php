<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Author;
use Faker\Generator as Faker;

use Illuminate\Support\Facades;


$factory->define(Author::class, function (Faker $faker) {
    return [
        'name' => $faker->name(1),
        'about' => $faker->text(35)
    ];
});
