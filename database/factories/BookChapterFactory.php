<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Book;
use App\Models\BookChapter;
use App\Models\BookSize;
use App\Models\Category;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use App\Models\Publisher;
use Faker\Generator as Faker;

use Illuminate\Support\Facades;


$factory->define(BookChapter::class, function (Faker $faker) {
    return [
        'book_id' => factory(Book::class)->create()->id,
        'name' => $faker->text(8)
    ];
});
