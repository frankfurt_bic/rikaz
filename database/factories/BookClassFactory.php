<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

use Illuminate\Support\Facades;


$factory->define(\App\Models\BookClass::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'number' => $faker->numberBetween(1000,10000),
    ];
});
