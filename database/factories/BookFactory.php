<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Author;
use App\Models\Book;
use App\Models\BookSize;
use App\Models\Category;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use App\Models\NumberingType;
use App\Models\Publisher;
use Faker\Generator as Faker;

use Illuminate\Support\Facades;


$factory->define(Book::class, function (Faker $faker) {

    $book_class_id = DB::table('book_class')->first()->id;
    $book_sub_class_id = DB::table('book_sub_class')->where('book_class_id',$book_class_id)->first()->id;

    return [
        'name' => $faker->name,
        'author_id' => factory(Author::class)->create()->id,
        'order' => $faker->numberBetween(0, 20),
       // 'numbering_type_id' =>  NumberingType::query()->first()->id,

        'book_class_id' => $book_class_id,
        'book_sub_class_id' => $book_sub_class_id,

        'ku_count' => 500,

        'book_identity_number' =>  '__' . $faker->numberBetween(1000000000,9999999999999999 ) ,
        'publisher_id' => Publisher::query()->count() == 0 ? factory(Publisher::class)->create()->id : Publisher::all()->first()->id,
        'edition_number' => $faker->numberBetween(0, 20),
        'language_id' => Language::query()->count() == 0 ? factory(Language::class)->create()->id : Language::all()->first()->id,
        'pages_count' => $faker->numberBetween(0, 20),
        'folders_count' => $faker->numberBetween(0, 20),
        'isbn' => (string)$faker->numberBetween(1000000000000, 9999999999999),
        'ikun' => $faker->numberBetween(0, 20),
        'cover_type_id' => CoverType::query()->count() == 0 ? factory(CoverType::class)->create()->id : CoverType::all()->first()->id,
        'book_size_id' => BookSize::query()->count() == 0 ? factory(BookSize::class)->create()->id : BookSize::all()->first()->id,
        'publication_year' => $faker->numberBetween(0, 20),
        'copy_type_id' => CopyType::query()->count() == 0 ? factory(CopyType::class)->create()->id : CopyType::all()->first()->id,
        'summary' => $faker->text(30),
       // 'category_ids' => [Category::first()->id]
    ];
});
