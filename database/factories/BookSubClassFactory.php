<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

use Illuminate\Support\Facades;


$factory->define(\App\Models\BookSubClass::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        'number' => $faker->numberBetween(1000,10000),
        'book_class_id'=>(\App\Models\BookClass::query()->inRandomOrder()->first()??factory(\App\Models\BookClass::class)->create())->id
    ];
});
