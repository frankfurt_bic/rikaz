<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BookChapter;
use App\Models\KnowledgeUnit;
use App\Model;
use App\Models\Book;
use Faker\Generator as Faker;

$factory->define(KnowledgeUnit::class, function (Faker $faker) {
    return [
        "header" => $faker->text(30),
        "summary" => $faker->text(55),
        "body" => $faker->text(200),
        "book_id" => factory(Book::class)->create()->id,
        "book_chapter_id" => factory(BookChapter::class)->create()->id,
        "number" => $faker->randomNumber(4),
        "full_number" => $faker->randomNumber(9)
    ];
});
