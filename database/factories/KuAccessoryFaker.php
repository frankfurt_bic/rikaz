<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\KnowledgeUnit;
use App\Models\KuAccessory;
use App\Models\KuAccessoryType;
use App\Model;
use Faker\Generator as Faker;

$factory->define(KuAccessory::class, function (Faker $faker) {
    return [
        "text" => $faker->text,
        "type_id" => KuAccessoryType::all()->random()->id,
        "knowledge_unit_id" => factory(KnowledgeUnit::class)->create()->id
    ];
});
