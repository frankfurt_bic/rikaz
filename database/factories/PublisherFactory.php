<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Publisher;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Publisher::class, function (Faker $faker) {
    $name = $faker->name;
    return [
        "name" => $name,
        "user_id" => factory(User::class)->create([
            "name" => $name
        ])->id,
    ];
});
