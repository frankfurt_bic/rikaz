<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('parent_id')->nullable(true);
            $table->Integer('position', false, true);
            $table->json('name');
            $table->string('notes')->nullable(true);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('parent_id')->references('id')
                ->on('categories')->onDelete('set null');
        });

        Schema::create('category_closure', function (Blueprint $table) {
            $table->increments('closure_id');

            $table->integer('ancestor', false, true);
            $table->integer('descendant', false, true);
            $table->integer('depth', false, true);

            $table->foreign('ancestor')->references('id')
                ->on('categories')->onDelete('cascade');

            $table->foreign('descendant')->references('id')
                ->on('categories')->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::dropIfExists('category_closure');
        Schema::dropIfExists('categories');

    }
}
