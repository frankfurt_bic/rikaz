<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_class', function (Blueprint $table) {
            $table->increments ('id');
            $table->unsignedInteger("number");
            $table->string("name");
            $table->timestamps();
        });

        Schema::create('book_sub_class', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->unsignedInteger("number");
            $table->unsignedInteger("book_class_id");
            $table->foreign('book_class_id')->references('id')->on('book_class');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_class');

        Schema::dropIfExists('book_sub_class');
    }
}
