<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNumberingType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numbering_type', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->timestamps();
        });

        Schema::create('numbering_type_range', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger("order");
            $table->unsignedInteger("digit_number");
            $table->string('type');
            $table->unsignedBigInteger("numbering_type_id");
            $table->foreign('numbering_type_id')->references('id')->on('numbering_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_ranges');
    }
}
