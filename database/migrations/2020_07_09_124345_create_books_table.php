<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable(False);
            $table->unsignedInteger('order')->nullable(True);
            $table->string('author')->nullable(False);

            $table->unsignedBigInteger('numbering_type_id')->nullable(true);
            $table->foreign('numbering_type_id')->references('id')->on('numbering_type');
            $table->string('book_identity_number')->nullable(false)->unique();

            $table->unsignedInteger('book_class_id')->nullable(False);
            $table->foreign('book_class_id')->references('id')->on('book_class');
            $table->unsignedInteger('book_sub_class_id')->nullable(False);
            $table->foreign('book_sub_class_id')->references('id')->on('book_sub_class');

            $table->unsignedInteger('ku_count')->nullable(False);

            $table->unsignedInteger('publisher_id')->nullable(False);
            $table->foreign('publisher_id')->references('id')->on('publishers');
            $table->unsignedInteger('edition_number')->nullable(False);
            $table->unsignedInteger('language_id')->nullable(False);
            $table->foreign('language_id')->references('id')->on('languages');
            $table->unsignedInteger('pages_count')->nullable(True);
            $table->unsignedInteger('folders_count')->nullable(True);
            $table->string('isbn')->nullable(True);
            $table->unsignedBigInteger('ikun')->nullable(True);
            $table->unsignedInteger('cover_type_id')->nullable(True);
            $table->foreign('cover_type_id')->references('id')->on('cover_types');
            $table->unsignedInteger('book_size_id')->nullable(True);
            $table->foreign('book_size_id')->references('id')->on('book_sizes');
            $table->unsignedInteger('publication_year')->nullable(True);
            $table->unsignedInteger('copy_type_id')->nullable(True);
            $table->foreign('copy_type_id')->references('id')->on('copy_types');
            $table->string('image')->nullable(True);
            $table->text('summary')->nullable(True);
            $table->boolean('publish_state')->default(0);
            $table->timestamps();
        });

        Schema::create('book_category', function (Blueprint $table) {
            $table->unsignedBigInteger('book_id')->nullable(False);
            $table->unsignedInteger('category_id')->nullable(False);
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('book_id')->references('id')->on('books')->onDelete('cascade');
            $table->primary(['book_id', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
