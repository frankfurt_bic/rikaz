<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksAppliedRangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applied_range', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('numbering_type_range_id')->nullable(False);
            $table->foreign('numbering_type_range_id')->references('id')->on('numbering_type_range');

            $table->unsignedBigInteger('book_id')->nullable(True);
            $table->foreign('book_id')->references('id')->on('books');
            $table->string("digit_number");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
