<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnowledgeUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_units', function (Blueprint $table) {
            $table->id();
            $table->string("number",15);
            $table->string("full_number",25);
            $table->string("header");
            $table->string('summary')->nullable(true);
            $table->longText("body");
            $table->unsignedBigInteger("book_id")->nullable(true);
            $table->boolean("publish_state")->default(0);
            $table->foreign("book_id")->references("id")->on("books");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledge_units');
    }
}
