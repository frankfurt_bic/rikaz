<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookChaptersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_chapters', function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedBigInteger("book_id");
            $table->unsignedInteger('parent_id')->nullable(true);
            $table->string("name");
            $table->timestamps();
        });
        Schema::table("book_chapters",function (Blueprint $table){
            $table->foreign("book_id")->references("id")->on("books");
        });
        Schema::create('book_chapters_closure', function (Blueprint $table) {
            $table->increments('closure_id');
            $table->integer('ancestor', false, true);
            $table->integer('descendant', false, true);
            $table->integer('depth', false, true);
            $table->foreign('ancestor')->references('id')
                ->on('book_chapters')->onDelete('cascade');
            $table->foreign('descendant')->references('id')
                ->on('book_chapters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_chapters');
        Schema::dropIfExists('book_chapters_closure');
    }
}
