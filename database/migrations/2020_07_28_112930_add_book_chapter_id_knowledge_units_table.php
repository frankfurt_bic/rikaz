<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBookChapterIdKnowledgeUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("knowledge_units",function (Blueprint $table){
            $table->unsignedInteger("book_chapter_id")->nullable(true);

            $table->foreign("book_chapter_id")->references("id")->on("book_chapters");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
