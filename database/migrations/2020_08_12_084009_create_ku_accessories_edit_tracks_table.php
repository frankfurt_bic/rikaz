<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKuAccessoriesEditTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ku_accessories_edit_tracks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("type_id");
            $table->text("text");
            $table->unsignedBigInteger("k_u_edit_track_id");
            $table->foreign("type_id")->references("id")->on("ku_accessory_types");
            $table->foreign("k_u_edit_track_id")->references("id")->on("k_u_edit_tracks");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ku_accessories_edit_tracks');
    }
}
