<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->id();
            $table->nullableMorphs('visitor');
            $table->morphs('content');
            $table->foreignId('country_id')->nullable(true);
            $table->foreignId('gender_id')->nullable(true);
            $table->bigInteger('count')->nullable(false)->default(1);
            $table->string('country_code')->nullable(true);
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
