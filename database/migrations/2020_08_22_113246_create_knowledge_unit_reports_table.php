<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKnowledgeUnitReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('knowledge_unit_reports', function (Blueprint $table) {
            $table->id();
            $table->text("report_text");
            $table->unsignedBigInteger("agent_id");
            $table->unsignedBigInteger("user_id")->nullable(true);
            $table->unsignedBigInteger("knowledge_unit_id");
            $table->string("reply_text")->nullable(true);
            $table->tinyInteger("status")->default(0);
            $table->foreign("agent_id")->references("id")->on("agents");
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("knowledge_unit_id")->references("id")->on("knowledge_units");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('knowledge_unit_reports');
    }
}
