<?php

use App\KuPublishRequest;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKuPublishRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ku_publish_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("knowledge_unit_id");
            $table->tinyInteger("state")->default(KuPublishRequest::WAITING);
            $table->foreign("knowledge_unit_id")->references("id")->on("knowledge_units");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ku_publish_requests');
    }
}
