<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveCountFromVisits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->dropColumn('count');
            $table->dropColumn('country_code');

        });
        Schema::table('visits', function (Blueprint $table) {
            $table->char('country_code',2)->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->dropColumn('country_code');

        });
        Schema::table('visits', function (Blueprint $table) {
            $table->bigInteger('count')->nullable(false)->default(1);
            $table->string('country_code')->nullable(true);

        });

    }
}
