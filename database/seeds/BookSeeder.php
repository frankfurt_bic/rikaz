<?php

use App\Models\Author;
use App\Models\Book;
use App\Models\BookSize;
use App\Models\Category;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use App\Models\Publisher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  $id = DB::table('book_type')->where('name' ,'masader')->first()->id;

        $author = new Author(["name" => "كاتب","about" => \Illuminate\Support\Str::random(50)]);
        $author->save();
        DB::table('numbering_type')->insert(['name' => 'ترقيم مصادر 4-1']);
        $numbering_type_id = DB::table('numbering_type')->where('name', 'ترقيم مصادر 4-1')->first()->id;
        DB::table('numbering_type_range')->insert(['order' => '1', 'digit_number' => 1, 'type' => 'bookscope', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '2', 'digit_number' => 2, 'type' => 'class', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '3', 'digit_number' => 1, 'type' => 'kucount', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '4', 'digit_number' => 3, 'type' => 'regagent', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '5', 'digit_number' => 4, 'type' => 'booknumber', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '6', 'digit_number' => 1, 'type' => 'kunumber', 'numbering_type_id' => $numbering_type_id]);


        DB::table('numbering_type')->insert(['name' => 'ترقيم مصادر 3-2']);
        $numbering_type_id = DB::table('numbering_type')->where('name', 'ترقيم مصادر 3-2')->first()->id;
        DB::table('numbering_type_range')->insert(['order' => '1', 'digit_number' => 1, 'type' => 'bookscope', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '2', 'digit_number' => 2, 'type' => 'class', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '3', 'digit_number' => 1, 'type' => 'kucount', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '4', 'digit_number' => 3, 'type' => 'regagent', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '5', 'digit_number' => 3, 'type' => 'booknumber', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '6', 'digit_number' => 2, 'type' => 'kunumber', 'numbering_type_id' => $numbering_type_id]);

        DB::table('numbering_type')->insert(['name' => 'ترقيم مصادر 2-3']);
        $numbering_type_id = DB::table('numbering_type')->where('name', 'ترقيم مصادر 2-3')->first()->id;
        DB::table('numbering_type_range')->insert(['order' => '1', 'digit_number' => 1, 'type' => 'bookscope', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '2', 'digit_number' => 2, 'type' => 'class', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '3', 'digit_number' => 1, 'type' => 'kucount', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '4', 'digit_number' => 3, 'type' => 'regagent', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '5', 'digit_number' => 2, 'type' => 'booknumber', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '6', 'digit_number' => 3, 'type' => 'kunumber', 'numbering_type_id' => $numbering_type_id]);

        DB::table('numbering_type')->insert(['name' => 'ترقيم مصادر 1-4']);
        $numbering_type_id = DB::table('numbering_type')->where('name', 'ترقيم مصادر 1-4')->first()->id;
        DB::table('numbering_type_range')->insert(['order' => '1', 'digit_number' => 1, 'type' => 'bookscope', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '2', 'digit_number' => 2, 'type' => 'class', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '3', 'digit_number' => 1, 'type' => 'kucount', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '4', 'digit_number' => 3, 'type' => 'regagent', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '5', 'digit_number' => 1, 'type' => 'booknumber', 'numbering_type_id' => $numbering_type_id]);
        DB::table('numbering_type_range')->insert(['order' => '6', 'digit_number' => 4, 'type' => 'kunumber', 'numbering_type_id' => $numbering_type_id]);

        $numbering_type_id = DB::table('numbering_type')->where('name', 'ترقيم مصادر 1-4')->first()->id;

        $book_class_id = DB::table('book_class')->first()->id;
        $book_sub_class_id = DB::table('book_sub_class')->where('book_class_id',$book_class_id)->first()->id;


        DB::table('books')->insert([
            'name' => "كتاب 1",
            'order' => "1",
            'author_id' => $author->id,

            'ku_count' => 500,

            'book_class_id' => $book_class_id,
            'book_sub_class_id' => $book_sub_class_id,
            'book_identity_number' => "133100011",
            'numbering_type_id' => $numbering_type_id,
            'publisher_id' => Publisher::query()->Where('name->ar', 'دار الفكر')->first()->id,
            'edition_number' => "1",
            'language_id' => Language::query()->Where('name', 'العربية')->first()->id,
            'pages_count' => "100",
            'folders_count' => "100",
            'isbn' => "1021231236545",
            'ikun' => "123456789012",
            'cover_type_id' => CoverType::query()->Where('name->ar', 'مجلد')->first()->id,
            'book_size_id' => BookSize::query()->Where('name->ar', 'كبير')->first()->id,
            'publication_year' => "2020",
            'copy_type_id' => CopyType::query()->Where('name->ar', 'ورقي')->first()->id,
            'image' => "/bookUploades",
            'summary' => "مختصر",
        ]);

        DB::table('book_category')->insert([
            'category_id' => Category::query()->where('name->ar', 'ديانة 1')->first()->id,
            'book_id' => Book::query()->where('name', 'كتاب 1')->first()->id,
        ]);

        $bookid = DB::table('books')->where('name','=', 'كتاب 1')->first()->id;

        $numbering_types = DB::table('numbering_type_range')->where('numbering_type_id', $numbering_type_id)->
        select('id')->orderBy('order')->get()->toArray();



        DB::table('applied_range')->insert(['numbering_type_range_id' => $numbering_types[0]->id, 'book_id' => $bookid, 'digit_number' => '1']);
        DB::table('applied_range')->insert(['numbering_type_range_id' => $numbering_types[1]->id, 'book_id' => $bookid, 'digit_number' => '53']);
        DB::table('applied_range')->insert(['numbering_type_range_id' => $numbering_types[2]->id, 'book_id' => $bookid, 'digit_number' => '2']);
        DB::table('applied_range')->insert(['numbering_type_range_id' => $numbering_types[3]->id, 'book_id' => $bookid, 'digit_number' => '001']);
        DB::table('applied_range')->insert(['numbering_type_range_id' => $numbering_types[4]->id, 'book_id' => $bookid, 'digit_number' => '001']);
        DB::table('applied_range')->insert(['numbering_type_range_id' => $numbering_types[5]->id, 'book_id' => $bookid, 'digit_number' => '']);


    }
}
