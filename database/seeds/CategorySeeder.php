<?php


use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use App\Models\CategoryClosure;
use Spatie\Translatable\HasTranslations;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app::setLocale('ar');

        $category0 = new Category(['name' => "المعارف العامة", 'notes' => ""]);
        $category0->setTranslation('name', 'en', 'GENERALITIES');
        $category0->save();

        $category1 = new Category(['name' => "الفلسفة وعلم النفس", 'notes' => ""]);
        $category1->setTranslation('name', 'en', 'PHILOSOPHY');
        $category1->save();

        $category2 = new Category(['name' => "الديانات", 'notes' => ""]);
        $category2->setTranslation('name', 'en', 'RELIGIONS');
        $category2->save();

        $category3 = new Category(['name' => "العلوم الاجتماعية", 'notes' => ""]);
        $category3->setTranslation('name', 'en', 'SOCIAL SCEINCE');
        $category3->save();

        $category4 = new Category(['name' => "اللّغات", 'notes' => ""]);
        $category4->setTranslation('name', 'en', 'LANGUAGES');
        $category4->save();

        $category5 = new Category(['name' => "العلوم البحتة", 'notes' => ""]);
        $category5->setTranslation('name', 'en', 'PURE SCEINCES');
        $category5->save();

        $category6 = new Category(['name' => "العلوم التطبيقية", 'notes' => ""]);
        $category6->setTranslation('name', 'en', 'APPLIED SCIENCES');
        $category6->save();

        $category7 = new Category(['name' => "الفنون والاستجمام والديكور", 'notes' => ""]);
        $category7->setTranslation('name', 'en', 'FINE ARTS');
        $category7->save();

        $category8 = new Category(['name' => "الآداب", 'notes' => ""]);
        $category8->setTranslation('name', 'en', 'LITERATURES');
        $category8->save();

        $category9 = new Category(['name' => "التاريخ، الجغرافيا والتراجم", 'notes' => ""]);
        $category9->setTranslation('name', 'en', 'HISTORY, GENERAL GEOGRAPHY etc');
        $category9->save();




      //------------------------------------------------------------------------------------


        $id = $category0->id;

        $category = new Category(['name' => "البيبليوغيرافيا ", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => " علم المكتبات والمعلومات", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الأعمال الموسوعية العامة ", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "المسلسلات (الدوريات) العامة وكشافاتها ", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => " المنظمات العامة وعلم المتاحف", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => " وسائل الإعلام، الصحافة، النشر", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => " المجموعات العامة", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => " المخطوطات والكتب النادرة", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

//---------------------------------------------------------------
        $id = $category1->id;

        $category = new Category(['name' => "الميتافيزيقا", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "نظرية المعرفة، السببية، الإنسان", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الظواهر غير الطبيعية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "المدارس الفلسفية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "علم النفس", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "المنطق", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الأخلاق", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الفلسفة القديمة والوسطى والشرقية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الفلسفة الغربية الحديثة", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();
//---------------------------------------------------------------

        $id = $category2->id;

        $category = new Category(['name' => "الإسلام وعلومه", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islam and its Sciences');
        $category->save();

        $category = new Category(['name' => "القرآن الكريم وعلومه", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'The Holy Quran and its Sciences');
        $category->save();

        $category = new Category(['name' => "الحديث الشريف وعلومه", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Hadith and its Sciences');
        $category->save();

        $category = new Category(['name' => "العقيدة الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islamic faith');
        $category->save();

        $category = new Category(['name' => "الفرق الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islamic teams');
        $category->save();

        $category = new Category(['name' => "الفقه الإسلامي", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Islamic jurisprudence');
        $category->save();

        $category = new Category(['name' => "المذاهب الفقهية الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', ' Islamic schools of jurisprudence');
        $category->save();

        $category = new Category(['name' => "الدفاع عن الإسلام، الأحزاب والحركات الإسلامية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', 'Defending Islam, which Islamic movements have');
        $category->save();

        $categoryx = new Category(['name' => "الديانات الأخرى", 'notes' => "", 'parent_id' => $id]);
        $categoryx->setTranslation('name', 'en', 'other religions');
        $categoryx->save();

        $categoryx = new Category(['name' => "الديانات الأخرى", 'notes' => "", 'parent_id' => $id]);
        $categoryx->setTranslation('name', 'en', 'other religions');
        $categoryx->save();


        $categoryx = new Category(['name' => "ديانة 1", 'notes' => "", 'parent_id' => $categoryx->id]);
        $categoryx->setTranslation('name', 'en', 'religion 1');
        $categoryx->save();

        $categoryx = new Category(['name' => "ديانة 2", 'notes' => "", 'parent_id' => $categoryx->id]);
        $categoryx->setTranslation('name', 'en', 'religion 2');
        $categoryx->save();

        $categoryx = new Category(['name' => "ديانة 3", 'notes' => "", 'parent_id' => $categoryx->id]);
        $categoryx->setTranslation('name', 'en', 'religion 3');
        $categoryx->save();

//---------------------------------------------------------------
        $id = $category3->id;

        $category = new Category(['name' => "الإحصاءات العامة", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "العلوم السياسية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الإقتصاد", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "القانون", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الإدارة العامة", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الخدمات الإجتماعية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "التربية والتعليم", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "التجارة، الإتصالات، النقل", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "العادات، آداب السلوك، الفلكلور", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        //---------------------------------------------------------------
        $id = $category4->id;

        $category = new Category(['name' => "اللغة العربية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "اللغة الإنكليزية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "اللغات الجرمانية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "اللغات الفرنسية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الإيطالية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الإسبانية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "اللاتينية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "اللغات الهللينية اليونانية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "اللغات الأخرى", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();


        //---------------------------------------------------------------
        $id = $category5->id;

        $category = new Category(['name' => "العلوم الطبيعية", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الرياضيات", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الفلك", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الفيزياء", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الكيمياء", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "علوم الأرض ", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "الأحافير", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "علم الأحياء", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

        $category = new Category(['name' => "علوم النبات", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();


        $category = new Category(['name' => "علوم الحيوان", 'notes' => "", 'parent_id' => $id]);
        $category->setTranslation('name', 'en', '');
        $category->save();

    }
}
