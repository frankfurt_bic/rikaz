<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table("countries")->insert([
            'name' => 'سوريا',
            'nationality_name'=>'سوري',
            'country_code'=>'SY'
        ]);
 DB::table("countries")->insert([
            'name' => 'المملكة العربية السعودية',
            'nationality_name'=>'سعودي',
            'country_code'=>'SA'
        ]);
 DB::table("countries")->insert([
            'name' => 'الولايات المتحدة الأمريكية',
            'nationality_name'=>'أمريكي',
            'country_code'=>'US'
        ]);
 DB::table("countries")->insert([
            'name' => 'تركيا',
            'nationality_name'=>'تركي',
            'country_code'=>'TR'
        ]);
 DB::table("countries")->insert([
            'name' => 'الإمارات العربية المتحدة',
            'nationality_name'=>'إماراتي',
            'country_code'=>'AE'
        ]);
 DB::table("countries")->insert([
            'name' => 'سلطنة عمان',
            'nationality_name'=>'عماني',
            'country_code'=>'OM'
        ]);
 DB::table("countries")->insert([
            'name' => 'قطر',
            'nationality_name'=>'قطري',
            'country_code'=>'QA'
        ]);
 DB::table("countries")->insert([
            'name' => 'البحرين',
            'nationality_name'=>'بحريني',
            'country_code'=>'BH'
        ]);
 DB::table("countries")->insert([
            'name' => 'العراق',
            'nationality_name'=>'عراقي',
            'country_code'=>'IQ'
        ]);
 DB::table("countries")->insert([
            'name' => 'إيران',
            'nationality_name'=>'إيراني',
            'country_code'=>'IR'
        ]);
 DB::table("countries")->insert([
            'name' => 'الكويت',
            'nationality_name'=>'كويتي',
            'country_code'=>'KW'
        ]);
 DB::table("countries")->insert([
            'name' => 'اليمن',
            'nationality_name'=>'يمني',
            'country_code'=>'YE'
        ]);
 DB::table("countries")->insert([
            'name' => 'مصر',
            'nationality_name'=>'مصري',
            'country_code'=>'EG'
        ]);
 DB::table("countries")->insert([
            'name' => 'لبنان',
            'nationality_name'=>'لبناني',
            'country_code'=>'LB'
        ]);
 DB::table("countries")->insert([
            'name' => 'الأردن',
            'nationality_name'=>'أردني',
            'country_code'=>'JO'
        ]);
 DB::table("countries")->insert([
            'name' => 'ألمانيا',
            'nationality_name'=>'ألماني',
            'country_code'=>'DE'
        ]);
DB::table("countries")->insert([
            'name' => 'المملكة المتحدة',
            'nationality_name'=>'بريطاني',
            'country_code'=>'GB'
        ]);

    }
}
