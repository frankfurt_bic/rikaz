<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CategorySeeder::class,
            PublisherSeeder::class,
            KuAccessoryTypesSeeder::class,
            LookUpsSeeder::class,
            BookSeeder::class,
            UsersSeeder::class,
            AuthorSeeder::class,
            FullBookSeeder::class
            ,CountrySeeder::class
            ,GenderSeeder::class
        ]);

    }
}
