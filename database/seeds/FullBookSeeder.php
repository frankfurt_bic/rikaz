<?php

use App\Http\Controllers\ApiControllers\BookApiController;
use App\Models\Author;
use App\Models\Book;
use App\Models\BookCategory;
use App\Models\BookSize;
use App\Models\Category;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use App\Models\NumberingType;
use App\Models\Publisher;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Laravel\Passport\Passport;

class FullBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app::setLocale('ar');
        $suffix = "->" . app::getLocale();
        $admin = User::where('email', '=', 'admin@gmail.com')->first();
        Passport::actingAs($admin);
        $numberingType = NumberingType::query()->first()->id;
        $category = Category::where('name' . $suffix, 'الإسلام وعلومه')->first();


        $author = Author::query()->where("name" . $suffix, "عائض القرني")->first();
        $imgname = 'latahzan.jpg';
        $this->prepareImage($imgname);
        $book = factory(Book::class)->create(['name' => 'لا تحزن', 'author_id' => $author->id, 'image' => '/storage/books_covers/' . $imgname,'summary'=> 'كتاب لا تحزن من تأليف عائض القرني ترجم للغات عدة. بلغت مبيعات الكتاب 10 ملايين نسخة']);
        $book->categories()->sync([$category->id]);
        $book->save();
        $this->numberBook($book, '2','001',$numberingType);


        $author = Author::query()->where('name' . $suffix, "جهاد الترباني")->first();
        $imgname = 'onehandred.jpg';
        $this->prepareImage($imgname);
        $book = factory(Book::class)->create(['name' => 'مئة من عظماء أمة الإسلام', 'author_id' => $author->id, 'image' => '/storage/books_covers/' . $imgname,'summary'=> 'مائة من عظماء أمة الإسلام غيروا مجرى التاريخ هو كتاب من تأليف الكاتب والباحث الفلسطيني جهاد الترباني، يتحدث عن 100 عظيم من عظماء الإسلام بعد الأنبياء، ويسلّط الضوء على عظماء الإسلام بالمفهوم الشامل (الاستسلام لله واتباع أوامره) وعظماء ما بعد وفاة النبي محمد صلى الله عليه وسلم إلى يومنا هذا مروراً بالخلافة الراشدة والخلافة الأموية والخلافة العباسية والخلافة العثمانية والمماليك والأيوبيون إلى يومنا هذا.']);
        $book->categories()->sync([$category->id]);
        $book->save();
        $this->numberBook($book, '2','001',$numberingType);


        $author = Author::query()->where("name" . $suffix, "ابراهيم الفقي")->first();
        $imgname = 'time.jpg';
        $this->prepareImage($imgname);
        $book = factory(Book::class)->create(['name' => 'الوقت', 'author_id' => $author->id, 'image' => '/storage/books_covers/' . $imgname,'summary'=> 'كتاب إدارة الوقـت يبين لنا مكانة الوقـت وأهميته، ويعرفنا على أسباب مضيعات الوقـت والتعرف على معوقات استغلال الوقـت، معرفة الـوقت المفقود ومدى نسبة الوقـت المستغل ويعطينا فوائد تنظيم الـوقت']);
        $book->categories()->sync([$category->id]);
        $book->save();
        $this->numberBook($book, '2','001',$numberingType);


        $author = Author::query()->where("name" . $suffix, "راغب السرجاني")->first();
        $imgname = 'andalus.jpg';
        $this->prepareImage($imgname);
        $book = factory(Book::class)->create(['name' => 'قصة الأندلس', 'author_id' => $author->id, 'image' => '/storage/books_covers/' . $imgname,'summary'=> 'عن الكتاب: يعتبر تاريخ الأندلس من أهم الفترات في التاريخ الإسلامي؛ حيث حكَمَ المُسلِمون الأندلس أكثر من ثمانية قرونٍ، ازدهَرَ فيها المُسلِمون ازدِهارًا كبيرًا، وتقدَّموا في جميع مناحِي الحياة تقدُّمًا أذهلَ العالم كلَّه، وحقَّقوا انتِصارات كثيرة على أعدائِهم، حتى وقعَت الانقسامات والانشقاقات بين صفوف المسلمين']);
        $book->categories()->sync([$category->id]);
        $book->save();


        $author = Author::query()->where("name" . $suffix, "ابو عبد الله البخاري")->first();
        $imgname = 'bokhari.jpg';
        $this->prepareImage($imgname);
        $book = factory(Book::class)->create(['name' => 'صحيح البهاري', 'author_id' => $author->id, 'image' => '/storage/books_covers/' . $imgname,'summary'=> '«الجامع المسند الصحيح المختصر من أُمور رسول الله صلى الله عليه وسلّم وسننه وأيامه»، الشهير بِاْسم «صحيح البخاري» هو أبرز كتب الحديث النبوي عند المسلمين من أهل السنة والجماعة. صنّفه الإمام محمد بن إسماعيل البخاري واستغرق في تحريره ستة عشر عاماً،']);
        $book->categories()->sync([$category->id]);
        $book->save();
    }

    /**
     * @param $book
     * @param $str
     */
    public function numberBook($book, $bookscope,$regagent,$numberingtypeid): void
    {
        $url = 'api/generateBookAppliedRange/' . $book->id;
        $data = ['bookscope' => $bookscope, 'regagent' => $regagent , 'numberingtypeid' =>$numberingtypeid ];

        $app = App::make('app');
        $req = Request::create($url, "POST", $data);

        $app->handle($req);
    }

    /**
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function prepareImage($imgname)
    {
        $bookImg = Storage::disk('seed_disk')->get($imgname);
        Storage::disk('public')->put("books_covers/{$imgname}", $bookImg);
    }
}
