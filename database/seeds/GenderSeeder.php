<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenderSeeder  extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table("genders")->insert([
            'name' => 'ذكر',

        ]);
        DB::table("genders")->insert([
            'name' => 'أنثى',

        ]);

    }
}
