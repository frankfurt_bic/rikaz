<?php

use App\Models\KuAccessoryType;
use Illuminate\Database\Seeder;

class KuAccessoryTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new KuAccessoryType(["name" => "هامش"]);
        $type->save();

        $type = new KuAccessoryType(["name" => "حاشية"]);
        $type->save();
    }
}
