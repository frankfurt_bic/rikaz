<?php

use App\Models\BookSize;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class LookUpsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app::setLocale('ar');

        $copyType = new CopyType(['name' => "رقمي"]);
        $copyType->setTranslation('name', 'en', 'Digital');
        $copyType->save();

        $copyType = new CopyType(['name' => "ورقي"]);
        $copyType->setTranslation('name', 'en', 'Paper');
        $copyType->save();

        $copyType = new CopyType(['name' => "رقمي وورقي"]);
        $copyType->setTranslation('name', 'en', 'Paper and Digital');
        $copyType->save();

        $coverType = new CoverType(['name' => "مجلد"]);
        $coverType->setTranslation('name', 'en', 'covered');
        $coverType->save();

        $coverType = new CoverType(['name' => "عادي"]);
        $coverType->setTranslation('name', 'en', 'normal');
        $coverType->save();

        $bookSize = new BookSize(['name' => "كبير"]);
        $bookSize->setTranslation('name', 'en', 'big');
        $bookSize->save();

        $bookSize = new BookSize(['name' => "صغير"]);
        $bookSize->setTranslation('name', 'en', 'small');
        $bookSize->save();


        $language = new Language(['name' => "العربية"]);
        $language->save();

        $language = new Language(['name' => "English"]);
        $language->save();


        DB::table('book_class')->insert(['number' => '1' ,'name' => 'المعارف العام']);
        DB::table('book_class')->insert(['number' => '2' ,'name' => 'الفلسفة وعلم النفس']);
        DB::table('book_class')->insert(['number' => '3' ,'name' => 'الديانات']);
        DB::table('book_class')->insert(['number' => '4' ,'name' => 'العلوم الاجتماعية']);
        DB::table('book_class')->insert(['number' => '5' ,'name' => 'اللّغات']);
        DB::table('book_class')->insert(['number' => '6' ,'name' => 'العلوم البحتة']);
        DB::table('book_class')->insert(['number' => '7' ,'name' => 'العلوم التطبيقية']);
        DB::table('book_class')->insert(['number' => '8' ,'name' => 'الفنون والاستجمام والديكور']);
        DB::table('book_class')->insert(['number' => '9' ,'name' => 'الآداب']);
        DB::table('book_class')->insert(['number' => '10' ,'name' => 'التاريخ، الجغرافيا والتراج']);

        DB::table('book_sub_class')->insert(['number' => '1','book_class_id'=> '1' ,'name' => 'البيبليوغيرافيا']);
        DB::table('book_sub_class')->insert(['number' => '2','book_class_id'=> '1' ,'name' => ' علم المكتبات والمعلومات']);
        DB::table('book_sub_class')->insert(['number' => '3','book_class_id'=> '1' ,'name' => 'الأعمال الموسوعية العامة']);
        DB::table('book_sub_class')->insert(['number' => '4','book_class_id'=> '1' ,'name' => 'المسلسلات (الدوريات) العامة وكشافاتها ']);
        DB::table('book_sub_class')->insert(['number' => '5','book_class_id'=> '1' ,'name' => ' المنظمات العامة وعلم المتاحف']);
        DB::table('book_sub_class')->insert(['number' => '6','book_class_id'=> '1' ,'name' => ' وسائل الإعلام، الصحافة، النشر']);
        DB::table('book_sub_class')->insert(['number' => '7','book_class_id'=> '1' ,'name' => ' المجموعات العامة']);
        DB::table('book_sub_class')->insert(['number' => '8','book_class_id'=> '1' ,'name' => ' المخطوطات والكتب النادرة']);


        DB::table('book_sub_class')->insert(['number' => '1','book_class_id'=> '2' ,'name' => 'الميتافيزيقا']);
        DB::table('book_sub_class')->insert(['number' => '2','book_class_id'=> '2' ,'name' => "نظرية المعرفة، السببية، الإنسان"]);
        DB::table('book_sub_class')->insert(['number' => '3','book_class_id'=> '2' ,'name' => "الظواهر غير الطبيعية"]);
        DB::table('book_sub_class')->insert(['number' => '4','book_class_id'=> '2' ,'name' => "المدارس الفلسفية"]);
        DB::table('book_sub_class')->insert(['number' => '5','book_class_id'=> '2' ,'name' => "علم النفس"]);
        DB::table('book_sub_class')->insert(['number' => '6','book_class_id'=> '2' ,'name' => "المنطق"]);
        DB::table('book_sub_class')->insert(['number' => '7','book_class_id'=> '2' ,'name' => "الأخلاق"]);
        DB::table('book_sub_class')->insert(['number' => '8','book_class_id'=> '2' ,'name' => "الفلسفة القديمة والوسطى والشرقية"]);
        DB::table('book_sub_class')->insert(['number' => '9','book_class_id'=> '2' ,'name' => "الفلسفة الغربية الحديثة"]);


        DB::table('book_sub_class')->insert(['number' => '1','book_class_id'=> '3' ,'name' => "الإسلام وعلومه"]);
        DB::table('book_sub_class')->insert(['number' => '2','book_class_id'=> '3' ,'name' => "القرآن الكريم وعلومه"]);
        DB::table('book_sub_class')->insert(['number' => '3','book_class_id'=> '3' ,'name' => "الحديث الشريف وعلومه"]);
        DB::table('book_sub_class')->insert(['number' => '4','book_class_id'=> '3' ,'name' => "العقيدة الإسلامية"]);
        DB::table('book_sub_class')->insert(['number' => '5','book_class_id'=> '3' ,'name' => "الفرق الإسلامية"]);
        DB::table('book_sub_class')->insert(['number' => '6','book_class_id'=> '3' ,'name' => "الفقه الإسلامي"]);
        DB::table('book_sub_class')->insert(['number' => '7','book_class_id'=> '3' ,'name' => "المذاهب الفقهية الإسلامية"]);
        DB::table('book_sub_class')->insert(['number' => '8','book_class_id'=> '3' ,'name' => "الدفاع عن الإسلام، الأحزاب والحركات الإسلامية"]);
        DB::table('book_sub_class')->insert(['number' => '9','book_class_id'=> '3' ,'name' => "الديانات الأخرى"]);
        DB::table('book_sub_class')->insert(['number' => '10','book_class_id'=> '3' ,'name' => "الديانات الأخرى"]);
    }
}
