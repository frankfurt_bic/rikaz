<?php

use App\Models\Agent;
use App\Models\Publisher;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\Artisan::call('passport:install');
        $admin =  User::create([
           "name" => "admin",
           "email" => "admin@gmail.com",
           "password" => Hash::make(12345678),
           "is_admin" => 1
        ]);
        $publisher = User::create([
            "name" => "publisher",
            "email" => "publisher@gmail.com",
            "password" => Hash::make(12345678)
        ]);
        $publisher = Publisher::create(['name' => "دار الفكر","user_id" => $publisher->id]);
        $publisher->setTranslation('name', 'en', 'Dar Al fiker');

        $agent = User::create([
            "name" => "agent",
            "email" => "agent@gmail.com",
            "password" => Hash::make(12345678)
        ]);
        $agent =  Agent::create(['first_name' => "agent",'last_name' => "agent","user_id" => $agent->id]);
    }
}
