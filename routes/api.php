<?php

use App\Models\BookClass;
use App\Models\BookSize;
use App\Models\CopyType;
use App\Models\CoverType;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Auth\\',
    'middleware' => ['always-json']
], function (Router $router) {

//    Agent Operations
    $router->post('/agent/registerByEmail', 'AgentApiController@registerByEmail');
    $router->get('/agent/{agent}/stats', 'AgentApiController@stats');

    $router->post('/user/loginByEmail', 'UserApiController@loginByEmail');
    $router->get('/user', 'UserApiController@getUser');
    $router->post('/user/socialLogin/{provider}', 'UserApiController@socialLogin');
    $router->post('/user/callBack_socialLogin/{provider}', 'UserApiController@callBack_socialLogin');
    $router->post('/user/forgetByEmail', 'UserApiController@forgetByEmail');
    $router->post('/user/resetByEmail', 'UserApiController@resetByEmail')->name("resetApi");
    $router->post('/user/refreshToken', 'UserApiController@refreshToken');
    $router->post('/user/deleteInstanceId', 'UserApiController@deleteInstanceId');
    $router->get('/user/verifyEmail', 'UserApiController@verifyEmail')->name("apiverify");

    $router->group(['middleware' => ['auth:api']],
        function (Router $router) {
            $router->post('/user/reSendMailVerificationCode', 'UserApiController@reSendMailVerificationCode');
            $router->post('/user/saveLocale', 'UserApiController@saveLocale');
            $router->get('/user/profile', 'UserApiController@profile');
            $router->put('user/update', 'UserApiController@update');
        });


});
Route::group(['namespace' => 'Auth\\','middleware' => [ 'always-json',"auth:api",'checkIfAdmin']], function (Router $router){
    //    Publisher Operations
    $router->post('/publisher/registerByEmail', 'PublisherApiController@registerByEmail');
//    Admin Operations
    $router->post('/admin/registerByEmail', 'AdminApiController@registerByEmail');
});
Route::group(['namespace' => 'Auth\\','middleware' => [ 'always-json',"auth:api",'checkIfAdmin',"checkIfActive","verified"]], function (Router $router){
//    Active toggle Operations
    $router->get('/user/toggleActive/{user}', 'UserApiController@toggleActive');
});

Route::group(['namespace' => 'ApiControllers\\admin\\'],function (Router $router){
    $router->apiResource('publisher', 'PublisherApiController');
    $router->apiResource('admin', 'AdminApiController');
    $router->apiResource('agent', 'AgentApiController');
});
Route::group(['namespace' => 'ApiControllers\\'],
    function (Router $router) {
        $router->apiResource('category', 'CategoryApiController');
        $router->get('relationalCategory', 'CategoryApiController@getRelationalCategory');
        $router->get("LeafCategories", "CategoryApiController@getLeafCategories");

        $router->apiResource('book', 'BookApiController');
        $router->get('book/{book}/stats', 'BookApiController@stats');
        $router->get('book/{book}/daily_stats', 'BookApiController@dailyStats');
        $router->get('book/{book}/monthly_stats', 'BookApiController@monthlyStats');
        $router->get('book/{book}/yearly_stats', 'BookApiController@yearlyStats');
        $router->get('categoryBooks/{id}', 'BookApiController@getCategoryBooks');

        $router->post('generateBookAppliedRange/{book}', 'BookApiController@generateBookAppliedRange');

        $router->get('nextBook/{book}', 'BookApiController@nextBook');
        $router->get('previousBook/{book}', 'BookApiController@previousBook');

        $router->post('addBookCategories/{bookId}', 'BookApiController@addBookCategories');
        $router->get("CopyTypes", "LookUpApiController@getCopyTypes");
        $router->get("CoverTypes", "LookUpApiController@getCoverTypes");
        $router->get("Languages", "LookUpApiController@getLanguages");
        $router->get("BookSizes", "LookUpApiController@getBookSizes");

        $router->get("KuAccessoriesType", "LookUpApiController@getKuAccessoriesType");

        $router->post("knowledge_unit/image", "KnowledgeUnitApiController@storeImage");
        $router->get("knowledge_unit/history/{knowledgeUnit}", "KnowledgeUnitApiController@getEditTrack");
        $router->get("knowledge_unit/audit/{knowledgeUnit}", "KnowledgeUnitApiController@getAudit");
        $router->apiResource("knowledge_unit", "KnowledgeUnitApiController");
        $router->get("knowledge_unit/{knowledgeUnit}/stats", "KnowledgeUnitApiController@stats");
        $router->get("knowledge_unit/{knowledgeUnit}/daily_stats", "KnowledgeUnitApiController@dailyStats");
        $router->get("knowledge_unit/{knowledgeUnit}/monthly_stats", "KnowledgeUnitApiController@monthlyStats");
        $router->get("knowledge_unit/{knowledgeUnit}/yearly_stats", "KnowledgeUnitApiController@yearlyStats");
        $router->apiResource("knowledge_unit_report", "KnowledgeUnitReportApiController");
        $router->apiResource("author", "AuthorApiController");
        $router->apiResource("book_publish_request", "BookPublishRequestApiController");

        $router->apiResource("ku_publish_request", "KuPublishRequestApiController");

        $router->post("ku_from_excel","KnowledgeUnitApiController@storeFromExcel");
        $router->get("book_tree/{book}","BookApiController@getTree");
        $router->apiResource("book_chapter","BookChapterApiController");
        $router->get("storage/{path}","LookUpApiController@getImage")->where('path', '(.*)');

        $router->get("BookClass", "LookUpApiController@getBookClass");
        $router->get("BookSubClass/{class_id}", "LookUpApiController@getBookSubClass");

        $router->get("NumberingType", "LookUpApiController@getNumberingTypes");
        $router->get("most_visited_books", "BookApiController@getMostVisitedBook");




});
