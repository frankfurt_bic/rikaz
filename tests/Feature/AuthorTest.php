<?php

namespace Tests\Feature;


use App\Models\Author;
use App\Models\Book;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class AuthorTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function storeAuthorTest(){
        $data = [
            "name" => Str::random(12),
            "about" => Str::random(40)
        ];
        $response = $this->postJson('api/author',$data);
        $response->assertSuccessful();
        $response->assertSee($data["name"]);
        $response2 = $this->get('api/author/'. $response["data"]["id"]);
        $response2->assertSuccessful();
        $response2->assertSee($data["name"]);
    }
    /** @test */
    public function updateAuthorTest(){
        $author = factory(Author::class)->create();
        $data = [
            "name" => Str::random(20),
            "about" => Str::random(50)
        ];
        $response = $this->putJson('api/author/'.$author->id,$data);
        $response->assertSuccessful();
        $response->assertSee($data["name"]);
    }
    /** @test */
    public function destoryAuthorTest(){
//        fail to delete author has a books
        $author = factory(Author::class)->create();
        $book = factory(Book::class)->create([
            "author_id" => $author->id
        ]);
        $response = $this->delete('api/author/'.$author->id);
        $response->assertSee('Can Not Delete Author Has A Books');
//        success to delete author has not a books
        $author = factory(Author::class)->create();
        $response = $this->delete('api/author/'.$author->id);
        $response->assertSuccessful();
    }
}
