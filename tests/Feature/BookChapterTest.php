<?php

namespace Tests\Feature;


use App\Models\BookChapter;
use App\Models\Book;
use App\Models\KnowledgeUnit;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class BookChapterTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function storeBookChapterTest(){
//        success store chapter with no parent
        $book = factory(Book::class)->create();
        $data = [
            "name" => Str::random(8),
            "book_id" => $book->id
        ];
        $response = $this->post("/api/book_chapter",$data);
        $response->assertSuccessful();
        $response->assertJsonPath("data.name",$data["name"]);
        $response->assertJsonPath("data.book_id",$data["book_id"]);
        $response->assertJsonPath("data.parent_id",null);
//        success store chapter with exists parent from same book
        $data2 = [
            "name" => Str::random(8),
            "book_id" => $book->id,
            "parent_id" => $response["data"]["id"]
        ];
        $response2 = $this->post("/api/book_chapter",$data2);
        $response2->assertSuccessful();
        $response2->assertJsonPath("data.name",$data2["name"]);
        $response2->assertJsonPath("data.book_id",$data2["book_id"]);
        $response2->assertJsonPath("data.parent_id",$response["data"]["id"]);
//        fail store chapter with exists parent from other book
        $book2 = factory(Book::class)->create();
//        get parent show to sure it is exists
        $parent = $this->get('api/book_chapter/' . $response["data"]["id"]);
        $parent->assertSuccessful();
        $parent->assertJsonPath('data.id',$response["data"]["id"]);
        $parent->assertJsonPath('data.name',$response["data"]["name"]);
        $parent->assertJsonPath('data.book_id',$response["data"]["book_id"]);
        $data2 = [
            "name" => Str::random(8),
            "book_id" => $book2->id,
            "parent_id" => $parent["data"]["id"]
        ];
        $response2 = $this->post("/api/book_chapter",$data2);
        $response2->assertSee('error');
        $response2->assertSee('parent_id');
    }

    /** @test */
    public function updateBookChapterTest(){
//        success update book chapter
        $chapter = factory(BookChapter::class)->create();
        $data = [
            "name" => Str::random(10)
        ];
        $response = $this->putJson('api/book_chapter/'.$chapter->id,$data);
        $response->assertSuccessful();
        $response->assertJsonPath("data.name",$data["name"]);
//        success update book chapter parent from same book
        $book = factory(Book::class)->create();
        $chapter1 = factory(BookChapter::class)->create([
            "book_id" => $book->id
        ]);
        $chapter2 = factory(BookChapter::class)->create([
            "book_id" => $book->id
        ]);
        $data = [
            "name" => Str::random(10),
            "parent_id" => $chapter2->id

        ];
        $response = $this->putJson('api/book_chapter/'.$chapter1->id,$data);
        $response->assertSuccessful();
        $response->assertJsonPath("data.name",$data["name"]);
        $response->assertJsonPath("data.parent_id",$data["parent_id"]);
//        fail update book chapter parent from other book
        $book1 = factory(Book::class)->create();
        $book2 = factory(Book::class)->create();
        $chapter1 = factory(BookChapter::class)->create([
            "book_id" => $book1->id
        ]);
        $chapter2 = factory(BookChapter::class)->create([
            "book_id" => $book2->id
        ]);
        $data = [
            "name" => Str::random(10),
            "parent_id" => $chapter2->id

        ];
        $response = $this->putJson('api/book_chapter/'.$chapter1->id,$data);
        $response->assertSee("error");
        $response->assertSee("parent_id");
    }

    /** @test */
    public function destroyBookChapterTest(){
//        success destroy chapter has not children
        $chapter = factory(BookChapter::class)->create();
        $response = $this->delete('api/book_chapter/'. $chapter->id);
        $response->assertSuccessful();
        $db_chapter = BookChapter::query()->find($chapter->id);
        $this->assertNull($db_chapter);
//        fail destroy chapter has children
        $chapter = factory(BookChapter::class)->create();
        $chapter2 = factory(BookChapter::class)->create([
            "parent_id" => $chapter->id
        ]);
        $response = $this->delete('api/book_chapter/'. $chapter->id);
        $response->assertStatus(401);
//        fail destroy chapter has knowledge units
        $chapter = factory(BookChapter::class)->create();
        $ku = factory(KnowledgeUnit::class)->create([
            "book_chapter_id" => $chapter->id
        ]);
        $response = $this->delete('api/book_chapter/'. $chapter->id);
        $response->assertStatus(401);
    }


    /** @test */
    public function testGetParentBookChaptersWithOutChildren(){
        $chapter = factory(BookChapter::class)->create();
        $chapter2 = factory(BookChapter::class)->create([
            "parent_id" => $chapter->id,
            "book_id" => $chapter->book_id
        ]);
        $chapter3 = factory(BookChapter::class)->create([
            "parent_id" => $chapter->id,
            "book_id" => $chapter->book_id
        ]);
        $res = $this->call("GET",'api/book_chapter',[
            "main" => true,
            "book_id" => $chapter->book_id
        ]);
        $res->assertSuccessful();
        $res->assertDontSee("children");
        $res->assertDontSee($chapter2->name);
        $res->assertDontSee($chapter3->name);
    }

}
