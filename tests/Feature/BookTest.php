<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\BookChapter;
use App\Models\Category;
use App\Models\KnowledgeUnit;
use App\Models\NumberingType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Ramsey\Uuid\Uuid;
use Tests\TestCase;


use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Http\Requests\BookRequest;
use App\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class BookTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function testBookStore()
    {
        app::setLocale('ar');

        $book = factory(Book::class)->make();
        $category = factory(Category::class)->create();

        $response = $this->post('api/book', array_merge(['category_ids' => [$category->id]], $book->toArray()));
        $response->assertSuccessful();

        $bookObj = Book::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($bookObj);
    }

    /** @test */
    public function testcategoryBooks()
    {
        $CategoryObj = factory(Category::class)->create(["name" => 'test']);

        $CategoryObj1 = factory(Category::class)->create(["name" => 'test1', "parent_id" => $CategoryObj->id]);
        $CategoryObj2 = factory(Category::class)->create(["name" => 'test2', "parent_id" => $CategoryObj->id]);

        $CategoryObj21 = factory(Category::class)->create(["name" => 'test2-1', "parent_id" => $CategoryObj2->id]);
        $CategoryObj22 = factory(Category::class)->create(["name" => 'test2-2', "parent_id" => $CategoryObj2->id]);

        $book1 = factory(Book::class)->create();
        $book2 = factory(Book::class)->create();
        $CategoryObj21->books()->sync([$book1->id, $book2->id]);

        $book3 = factory(Book::class)->create();
        $book4 = factory(Book::class)->create();

        $CategoryObj22->books()->sync([$book3->id, $book4->id]);

        $CategoryObj->books()->sync([$book3->id, $book4->id]);

        $response = $this->json('get', 'api/categoryBooks/' . $CategoryObj->id);

        $response->assertJsonCount(4, 'data');
    }

    public function testaddBookCategories()
    {

        $collection = factory(Category::class, 3)->create(["name" => 'test']);
        $book1 = factory(Book::class)->create(); //['category_id' => $CategoryObj21->id]


        $p = $this->post('api/addBookCategories/' . $book1->id, ['categories_ids' => $collection->pluck('id')->toArray()]);
        $p->assertSuccessful();
        $book1 = Book::find($book1->id);
        $ids = $book1->categories()->pluck('id');
        $this->assertCount(3, $ids);
    }

    /** @test */
    public function storeBookWithCategoriesTest()
    {
        $category = factory(Category::class)->create(["name" => 'test']);
        $book = factory(Book::class)->make()->toArray();
        $book["category_ids"] = [$category->id];

        $response = $this->post("api/book", $book);
        $response->assertSuccessful();
        $added_book = Book::query()->find($response["data"]["id"]);
        $this->assertCount(1, $added_book->categories);
        $this->assertEquals($added_book->categories[0]->id, $category->id);
    }

    /** @test */
    public function updateBookWithCategoriesTest()
    {
        $category1 = factory(Category::class)->create(["name" => 'test1']);
        $category2 = factory(Category::class)->create(["name" => 'test2']);
        $category3 = factory(Category::class)->create(["name" => 'test3']);
        $book = factory(Book::class)->make()->toArray();
        $book["category_ids"] = [$category1->id];

        $response = $this->post("api/book", $book);
        $response->assertSuccessful();
        $added_book = Book::query()->find($response["data"]["id"]);
        $this->assertCount(1, $added_book->categories);
        $this->assertEquals($added_book->categories[0]->id, $category1->id);

        $book['id'] = $added_book->id;
        $book["category_ids"] = [$category2->id, $category3->id];
        $response = $this->put("api/book/$added_book->id", $book, [
            "Accept" => 'application/json'
        ]);
        $response->assertSuccessful();
        $added_book = Book::query()->find($response["data"]["id"]);
        $this->assertCount(2, $added_book->categories);
        $this->assertContains($category3->id, $added_book->categories()->pluck("id")->toArray());
        $this->assertContains($category2->id, $added_book->categories()->pluck("id")->toArray());

        $this->assertNotContains($category1->id, $added_book->categories()->pluck("id")->toArray());
    }

    /** @test */
    public function destroyBook()
    {
        $book = factory(Book::class)->create();
        $category = factory(Category::class)->create();
        $chapter = factory(Models\BookChapter::class)->create([
            "book_id" => $book->id
        ]);
        $ku = factory(Models\KnowledgeUnit::class)->create([
            "book_id" => $book->id,
            "book_chapter_id" => $chapter->id,
        ]);
        $book->categories()->sync([$category->id]);
        $book->save();
        $response = $this->delete('api/book/' . $book->id);
        $response->assertSuccessful();
        $book2 = Book::find($book->id);
        $this->assertNull($book2);
        $book_category = DB::table('book_category')->where("category_id", $category->id)->where("book_id", $book->id)->first();
        $this->assertNull($book_category);
    }

    /** @test */
    public function testgenerateBookAppliedRange()
    {
        $book = factory(Book::class)->create();

        $response = $this->post('api/generateBookAppliedRange/' . $book->id, ['bookscope' => '2', 'regagent' => '001', 'numberingtypeid' => NumberingType::query()->first()->id]);
        $response->assertSuccessful();
    }

    /** @test */
    public function testStoreBookInUnemptyCategory()
    {
        $category = factory(Category::class)->create();
        $category1 = factory(Category::class)->create(['parent_id' => $category->id]);
        $book = factory(Book::class)->make();
        $response = $this->post('api/book', array_merge(['category_ids' => [$category->id]], $book->toArray()));
        $response->assertSee('could not add because parent category have categories');

    }

    /** @test */
    public function testGetBookWithFilters()
    {
        $publisher = factory(Models\Publisher::class)->create();
        $book1 = factory(Book::class)->create([
            "publisher_id" => $publisher->id
        ]);
        $author = factory(Models\Author::class)->create();
        $book2 = factory(Book::class)->create([
            "author_id" => $author->id
        ]);
        $book3 = factory(Book::class)->create([
            "book_identity_number" => \Ramsey\Uuid\Uuid::uuid1()->toString()
        ]);
        $book4 = factory(Book::class)->create([
            "publish_state" => Book::PUBLISHED
        ]);
        $book5 = factory(Book::class)->create([
            "publish_state" => Book::WAITING
        ]);

        $res = $this->call('GET', 'api/book', [
            "author_id" => $author->id
        ]);
        $res->assertSuccessful();
        $res->assertSee($book2->name);
        $res->assertDontSee($book1->name);
        $res->assertDontSee($book3->name);

        $res2 = $this->call('GET', 'api/book', [
            "publisher_id" => $publisher->id
        ]);
        $res2->assertSuccessful();
        $res2->assertSee($book1->name);
        $res2->assertDontSee($book2->name);
        $res2->assertDontSee($book3->name);

        $res3 = $this->call('GET', 'api/book', [
            "numbering_state" => true
        ]);
        $res3->assertSuccessful();
        $res3->assertSee($book3->name);
        $res3->assertDontSee($book2->name);
        $res3->assertDontSee($book1->name);

        $res4 = $this->call('GET', 'api/book', [
            "numbering_state" => false
        ]);
        $res4->assertSuccessful();
        $res4->assertDontSee($book3->name);
        $res4->assertSee($book2->name);
        $res4->assertSee($book1->name);

        $res5 = $this->call('GET', 'api/book', [
            "publish_state" => true
        ]);
        $res5->assertSuccessful();
        $res5->assertSee($book4->name);
        $res5->assertDontSee($book5->name);
        $res5->assertDontSee($book3->name);;
        $res5->assertDontSee($book2->name);
        $res5->assertDontSee($book1->name);


    }


    /** @test */
    public function testGetBookTree()
    {
        $book = factory(Book::class)->create();
        $chapter = factory(BookChapter::class)->create([
            "book_id" => $book->id
        ]);
        $chapter2 = factory(BookChapter::class)->create([
            "parent_id" => $chapter->id,
            "book_id" => $book->id
        ]);
        $chapter5 = factory(BookChapter::class)->create([
            "parent_id" => $chapter2->id,
            "book_id" => $book->id
        ]);
        $chapter3 = factory(BookChapter::class)->create([
            "parent_id" => $chapter->id,
            "book_id" => $book->id
        ]);
        $unit = factory(KnowledgeUnit::class)->create([
            "book_id" => $book->id,
            "book_chapter_id" => $chapter->id
        ]);
        $unit2 = factory(KnowledgeUnit::class)->create([
            "book_id" => $book->id,
            "book_chapter_id" => null
        ]);

        $res = $this->get("api/book_tree/$book->id");
        $res->assertSuccessful();
        $res->assertSee($chapter2->name);
        $res->assertSee($chapter3->name);
        $res->assertSee($unit->header);
        $res->assertSee($unit2->header);
    }


    /** @test */
    public function testShow()
    {
        $book = factory(Book::class)->create();
        $responce = $this->get("api/book/$book->id");
        $this->assertNotNull($responce);
    }
    /** @test */
    public function testNextBook()
    {
        $publisher = factory(Models\Publisher::class)->create();
        $publisher2 = factory(Models\Publisher::class)->create();
        $book1 = factory(Book::class)->create(["publisher_id" => $publisher->id,"order" => 501]);
        $book2 = factory(Book::class)->create(["publisher_id" => $publisher2->id,"order" => 502]);
        $book3 = factory(Book::class)->create(["publisher_id" => $publisher->id,"order" => 503]);

        $response = $this->call('GET', 'api/nextBook/'. $book1->id, [
            "type" => "publisher",
            "publisher_id" => $publisher->id
        ]);
        $response->assertSuccessful();
        $response->assertSee($book3->fresh()->id);
    }
    /** @test */
    public function testPreviousBook()
    {
        $publisher = factory(Models\Publisher::class)->create();
        $publisher2 = factory(Models\Publisher::class)->create();
        $book1 = factory(Book::class)->create(["publisher_id" => $publisher->id,"order" => 501]);
        $book2 = factory(Book::class)->create(["publisher_id" => $publisher2->id,"order" => 502]);
        $book3 = factory(Book::class)->create(["publisher_id" => $publisher->id,"order" => 503]);

        $response = $this->call('GET', 'api/previousBook/'. $book3->id, [
            "type" => "publisher",
            "publisher_id" => $publisher->id
        ]);
        $response->assertSuccessful();
        $response->assertSee($book1->fresh()->id);
    }

}
