<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function testCategoryStore()
    {
        app::setLocale('ar');

        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();
        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);
    }

    /** @test */
    public function testUnStoredCategory()
    {
        $result = $this->json('post', 'api/category',  ["name"=> 'test', "notes" => 'test','parent_id' => -1255]);
        $result->assertStatus(422);
    }

    /** @test */
    public function testTranslationCategory()
    {
        App::setLocale('ar');
        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();

        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);
    }

    /** @test */
    public function testRelationalCategory()
    {
        $response =  $this->get('api/relationalCategory');
        $response->assertSuccessful();
    }

    /** @test */
    public function testDestroyCategory()
    {
        app::setLocale('ar');

        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();
        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);

        $response =   $this->delete('api/category/'. $CategoryObj->id);
        $response->assertSuccessful();
    }
    /** @test */
    public function testShowCategory()
    {
        app::setLocale('ar');

        $response = $this->post('api/category', ["name"=> 'test', "notes" => 'test']);
        $response->assertSuccessful();

        $CategoryObj = Category::query()->firstWhere('id', $response['data']['id']);
        $this->assertNotNull($CategoryObj);

        $response = $this->get('api/category/'.$CategoryObj->id);
        $response->assertSuccessful();
    }

    /** @test */
    public function testStoreCategoryInBookCatrogry()
    {
        app::setLocale('ar');

        $category = factory(Category::class)->create();
        $book = factory(Book::class)->create();
        $book->categories()->sync([$category->id]);
        $book->save();

        $category1 = factory(Category::class)->make(["name" => 'test','parent_id'=>$category->id]);

        $response = $this->post('api/category', ["name"=> 'test','parent_id'=>$category->id]);
        $response->assertSee('could not add because parent category have books');
    }



    /** @test */
    public function testLeafCategories()
    {
        app::setLocale('ar');
        $response = $this->get('api/LeafCategories');
        $responseData = json_decode($response->baseResponse->content())->data;
        $response->assertSuccessful();
    }


    /** @test */
    public function testno_children()
    {
        app::setLocale('ar');
        $response = $this->get('api/category?no_children=1');
        $responseData = json_decode($response->baseResponse->content())->data;
        $response->assertSuccessful();
    }
    /** @test */
    public function testShowCategoryWithBooks()
    {
        app::setLocale('ar');
        $response = $this->get('api/category/11?with_books=true');
        $response->assertSuccessful();
        $response->assertSee('books');
    }


}
