<?php

namespace Tests\Feature;


use App\KnowledgeUnitReport;
use App\Models\Agent;
use App\Models\Author;
use App\Models\Book;
use App\Models\KnowledgeUnit;
use App\Models\Publisher;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class KnowledgeUnitReportTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function successAgentReportKnowledgeUnit(){
        $ku = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
          "knowledge_unit_id" => $ku->id,
          "report_text" => Str::random(55)
        ];
        $res = $this->postJson('api/knowledge_unit_report',$data);
        $res->assertSuccessful();
        $res->assertSee($ku->id);
        $res->assertSee($data['report_text']);

    }
    /** @test */
    public function successAdminIndexAllKnowledgeUnitReports(){
        $ku = factory(KnowledgeUnit::class)->create();
        $ku2 = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $data2 = [
            "knowledge_unit_id" => $ku2->id,
            "report_text" => Str::random(55)
        ];
        $this->postJson('api/knowledge_unit_report',$data);
        $this->postJson('api/knowledge_unit_report',$data2);
        $admin = factory(User::class)->create([
            "is_admin" => 1
        ]);
        $this->actingAs($admin,'api');
        $res2 = $this->call('GET','api/knowledge_unit_report');
        $res2->assertSuccessful();
        $res2->assertSee($data['report_text']);
        $res2->assertSee($data2['report_text']);

    }
    /** @test */
    public function successAgentIndexOwnKnowledgeUnitReports(){
        $ku = factory(KnowledgeUnit::class)->create();
        $ku2 = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $agent2 = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $data2 = [
            "knowledge_unit_id" => $ku2->id,
            "report_text" => Str::random(55)
        ];
        $this->postJson('api/knowledge_unit_report',$data);

        $this->actingAs($agent2->user,'api');
        $this->postJson('api/knowledge_unit_report',$data2);
        $res2 = $this->call('GET','api/knowledge_unit_report');
        $res2->assertSuccessful();
        $res2->assertSee($data2['report_text']);
        $res2->assertDontSee($data['report_text']);

    }
    /** @test */
    public function successAgentShowOwnKnowledgeUnitReport(){
        $ku = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $res = $this->postJson('api/knowledge_unit_report',$data);
        $res2 = $this->call('GET','api/knowledge_unit_report/' . $res["data"]["id"]);
        $res2->assertSuccessful();
        $res2->assertSee($data['report_text']);

    }
    /** @test */
    public function failAgentShowNotOwnKnowledgeUnitReport(){
        $ku = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $agent2 = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $res = $this->postJson('api/knowledge_unit_report',$data);

        $this->actingAs($agent2->user,'api');
        $res2 = $this->call('GET','api/knowledge_unit_report/' . $res["data"]["id"]);
        $res2->assertStatus(404);
    }
    /** @test */
    public function successAdminUpdateReportStatus(){
        $admin = factory(User::class)->create([
            "is_admin" => 1
        ]);
        $ku = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $data2 = [
            "approved" => true,
            "reply_text" => Str::random(55)
        ];
        $res = $this->postJson('api/knowledge_unit_report',$data);
        $this->actingAs($admin,'api');
        $res2 = $this->putJson('api/knowledge_unit_report/'.$res['data']['id'],$data2);
        $res2->assertSuccessful();
        $res2->assertJsonPath('data.status',KnowledgeUnitReport::APPROVED);
        $res2->assertJsonPath('data.reply_text',$data2['reply_text']);

    }
    /** @test */
    public function failNotAdminUpdateReportStatus(){
        $ku = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $data2 = [
            "approved" => true,
            "reply_text" => Str::random(55)
        ];
        $res = $this->postJson('api/knowledge_unit_report',$data);
        $res2 = $this->putJson('api/knowledge_unit_report/'.$res['data']['id'],$data2);
        $res2->assertStatus(403);
        $publisher = factory(Publisher::class)->create();
        $this->actingAs($publisher->user,'api');
        $res2 = $this->putJson('api/knowledge_unit_report/'.$res['data']['id'],$data2);
        $res2->assertStatus(403);
    }
    /** @test */
    public function testIndexFilterAndSearch(){
        $ku = factory(KnowledgeUnit::class)->create();
        $agent = factory(Agent::class)->create();
        $this->actingAs($agent->user,'api');
        $data = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $data2 = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $data3 = [
            "knowledge_unit_id" => $ku->id,
            "report_text" => Str::random(55)
        ];
        $admin_data = [
            "approved" => true,
            "reply_text" => Str::random(55)
        ];
        $admin_data2 = [
            "approved" => false,
            "reply_text" => Str::random(55)
        ];
        $res = $this->postJson('api/knowledge_unit_report',$data);
        $res2 = $this->postJson('api/knowledge_unit_report',$data2);
        $res3 = $this->postJson('api/knowledge_unit_report',$data3);
        $admin = factory(User::class)->create([
            "is_admin" => 1
        ]);
        $this->actingAs($admin,'api');
        $res4 = $this->putJson('api/knowledge_unit_report/'.$res['data']['id'],$admin_data);
        $res5 = $this->putJson('api/knowledge_unit_report/'.$res2['data']['id'],$admin_data2);
        $res6 = $this->call('GET','api/knowledge_unit_report',[
            'status' => 'approved'
        ]);
        $res6->assertSee($admin_data['reply_text']);
        $res6->assertSee($res['data']['report_text']);
        $res6->assertDontSee($admin_data2['reply_text']);
        $res6 = $this->call('GET','api/knowledge_unit_report',[
            'status' => 'aborted'
        ]);
        $res6->assertSee($admin_data2['reply_text']);
        $res6->assertSee($res2['data']['report_text']);
        $res6->assertDontSee($admin_data['reply_text']);
        $res6 = $this->call('GET','api/knowledge_unit_report',[
            'status' => 'waiting'
        ]);
        $res6->assertDontSee($admin_data2['reply_text']);
        $res6->assertSee($res3['data']['report_text']);
        $res6->assertDontSee($res['data']['report_text']);
        $res6->assertDontSee($res2['data']['report_text']);
        $res6->assertDontSee($admin_data['reply_text']);

        $res6 = $this->call('GET','api/knowledge_unit_report',[
            'search' => $res3['data']['report_text']
        ]);
        $res6->assertSee($res3['data']['report_text']);
        $res6->assertDontSee($res2['data']['report_text']);

        $res6 = $this->call('GET','api/knowledge_unit_report',[
            'search' => $ku->header
        ]);
        $res6->assertSee($res3['data']['report_text']);
        $res6->assertSee($res2['data']['report_text']);
        $res6->assertSee($res['data']['report_text']);

    }

}
