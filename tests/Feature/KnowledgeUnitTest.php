<?php

namespace Tests\Feature;


use App\KuAccessoriesEditTrack;
use App\KUEditTrack;
use App\Models\BookChapter;
use App\Models\Category;
use App\Models\KnowledgeUnit;
use App\Models\KuAccessory;
use App\Models\KuAccessoryType;
use App\Models\Book;
use App\Models\NumberingType;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use OwenIt\Auditing\Audit;
use Tests\TestCase;

class KnowledgeUnitTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function testKnowledgeUnitStoreWithAccessories()
    {
        app::setLocale('ar');
//        Test 1 : success to add KnowledgeUnit and see it with book show
        $book = factory(Book::class)->create([
            "numbering_type_id" => 1
        ]);
        $book_chapter = factory(BookChapter::class)->create(["book_id" => $book->id]);
        $type1 = factory(KuAccessoryType::class)->create();
        $type2 = factory(KuAccessoryType::class)->create();
        $type3 = factory(KuAccessoryType::class)->create();
        $kuData1 = [
            "header" => Str::random(20),
            "summary" => Str::random(55),
            "body" => Str::random(100),
            "book_id" => $book->id,
            "accessories" => [
                json_encode([
                    "text" => Str::random(35),
                    "type_id" => $type1->id
                ])
            ],
        ];

        $response = $this->post('api/generateBookAppliedRange/' . $book->id, [
            'bookscope'=> "1",
            'regagent' => '001',
            'numberingtypeid' => NumberingType::query()->first()->id
        ]);
        $response->assertSuccessful();

        $response1 = $this->postJson("/api/knowledge_unit",$kuData1);
        $response1->assertSuccessful();
        $freshBook = $this->get("/api/book/$book->id");
        $freshBook->assertSee($kuData1["header"]);
//        test if accessories count equal to accessories added count
        $this->assertCount(count($kuData1["accessories"]),$freshBook["data"]["knowledge_units"][0]["accessories"]);
//        Test 2 : success to add KnowledgeUnit to book chapter and see it with book chapter show
        $book = factory(Book::class)->create();
        $book_chapter = factory(BookChapter::class)->create(["book_id" => $book->id]);
        $type1 = factory(KuAccessoryType::class)->create();
        $type2 = factory(KuAccessoryType::class)->create();
        $type3 = factory(KuAccessoryType::class)->create();
        $kuData1 = [
            "header" => Str::random(20),
            "summary" => Str::random(55),
            "body" => Str::random(100),
            "book_chapter_id" => $book_chapter->id,
            "accessories" => [
                    json_encode([
                        "text" => Str::random(35),
                        "type_id" => $type1->id
                    ]),
                    json_encode([
                        "text" => Str::random(35),
                        "type_id" => $type2->id
                    ]),
                    json_encode([
                        "text" => Str::random(35),
                        "type_id" => $type3->id
                    ]),
            ],
        ];
        $response = $this->post('api/generateBookAppliedRange/' . $book->id, [
            'bookscope'=> "1",
            'regagent' => '001',
            'numberingtypeid' => NumberingType::query()->first()->id
        ]);
        $response->assertSuccessful();
        $response1 = $this->postJson("/api/knowledge_unit",$kuData1);
        $response1->assertSuccessful();
        $freshBookChapter = $this->get("/api/book_chapter/$book_chapter->id");
        $freshBookChapter->assertSee($kuData1["header"]);
//        test if accessories count equal to accessories added count
        $this->assertCount(count($kuData1["accessories"]),$freshBookChapter["data"]["knowledge_units"][0]["accessories"]);
    }
    /** @test */
    public function testUpdateKnowledgeUnitWithAccessories(){
//        success update the knowledge accessories
        $accessory1 = factory(KuAccessory::class)->create();
        $unit1 = $accessory1->knowledgeUnit;
        $chapter1 = $unit1->bookChapter;
        $type1 = factory(KuAccessoryType::class)->create();
        $type2 = factory(KuAccessoryType::class)->create();
        $unit1Data = [
            "header" =>  Str::random(20),
            "summary" => Str::random(55),
            "body" => Str::random(100),
            "book_chapter_id" => $chapter1->id,
            "accessories" => [
                json_encode([
                    "text" => Str::random(35),
                    "type_id" => $type2->id
                ])
            ]
        ];
        $response1 = $this->putJson("/api/knowledge_unit/$unit1->id",$unit1Data);
        $response1->assertSuccessful();
        $response1->assertSee(json_decode($unit1Data["accessories"][0])->text);
        $response1->assertSee($unit1Data["header"]);
//        $editTrack = $this->get('api/knowledge_unit/history/'.$response1["data"]["id"]);
//        $this->assertNotEquals($editTrack["data"]["header"], $unit1->header);
//        $this->assertContains( $unit1->header,collect($editTrack["data"]["edits_track"])->pluck("header"));


//        success update the knowledge accessories with exists types but delete old
        $response2 = $this->putJson("/api/knowledge_unit/$unit1->id",$unit1Data);
        $response2->assertSuccessful();
        $response2->assertSee(json_decode($unit1Data["accessories"][0])->text);
        $response2->assertSee($unit1Data["header"]);
//        $editTrack = $this->get('api/knowledge_unit/history/'.$response2["data"]["id"]);
//        $editTrack->assertSee( $unit1->header);
//        $editTrack->assertSee($accessory1->text);

//        fail update the knowledge accessories with exists types without delete old
//        $unit1Data["deleted_accessories"] = [];
//        $response2 = $this->putJson("/api/knowledge_unit/$unit1->id",$unit1Data);
//        $response2->assertSee("errors");
//        $response2->assertSee("type_ids");

    }
    public function testAuditKnowledgeUnitChanges(){
        Passport::actingAs(factory(User::class)->create(['is_admin'=>true]));
        $Data = [
            "header" => Str::random(20),
            "summary" => Str::random(55),
            "body" => Str::random(100),
            "book_id" => factory(Book::class)->create(["numbering_type_id" => 1])->id,
            "accessories" => [
                json_encode([
                    "text" => Str::random(35),
                    "type_id" => factory(KuAccessoryType::class)->create()->id
                ])
            ],
        ];
        $this->post('api/generateBookAppliedRange/' . $Data['book_id'], [
            'bookscope'=> "1",
            'regagent' => '001',
            'numberingtypeid' => NumberingType::query()->first()->id
        ]);
        factory(BookChapter::class)->create(["book_id" => $Data['book_id']]);

        $response = $this->postJson("/api/knowledge_unit",$Data);
        $response->assertSuccessful();
        $resultArray = (array)$response->decodeResponseJson();

        $knowledgeUnit =  KnowledgeUnit::find($resultArray['data'] ['id']);
        $data1 = [
            "header" =>  Str::random(20),
            "summary" => Str::random(55),
            "body" => Str::random(100),

        ];
        $response1 = $this->putJson("/api/knowledge_unit/$knowledgeUnit->id",array_merge($data1,["accessories" => [
            json_encode([
                "text" => Str::random(35),
                "type_id" => factory(KuAccessoryType::class)->create()->id
            ])
        ]]));
        $response1->assertSuccessful();
        $response1->assertSee($data1["header"]);
        $data2 = $data1;
//            [
//            "header" =>  Str::random(20),
//            "summary" => Str::random(55),
//            "body" => Str::random(100),
//
//        ];
        $response2 = $this->putJson("/api/knowledge_unit/$knowledgeUnit->id",array_merge($data2,["accessories" => [
            json_encode([
                "text" => Str::random(35),
                "type_id" => factory(KuAccessoryType::class)->create()->id
            ]), json_encode([
                "text" => Str::random(35),
                "type_id" => factory(KuAccessoryType::class)->create()->id
            ]), json_encode([
                "text" => Str::random(35),
                "type_id" => factory(KuAccessoryType::class)->create()->id
            ]), json_encode([
                "text" => Str::random(35),
                "type_id" => factory(KuAccessoryType::class)->create()->id
            ]),
        ]]));
        $response2->assertSuccessful();
        $response2->assertSee($data2["header"]);

        $result = $this->json('get', "/api/knowledge_unit/audit/$knowledgeUnit->id");
        $resultArray = (array)$result->decodeResponseJson();
        $result->assertSuccessful();
        $resultKU =  $resultArray['data'] ;
        self::assertCount(3,$resultKU);
        self::assertCount(1,$resultKU[0]['accessories']);
        self::assertCount(2,$resultKU[1]['accessories']);
        self::assertCount(5,$resultKU[2]['accessories']);
    }

    /** @test */
    public function testDestroyKnowledgeUnitWithAccessories(){
        $unit = factory(KuAccessory::class)->create()->knowledgeUnit;
        $accessories = KuAccessory::query()->where("knowledge_unit_id",$unit->id)->get();
        $this->assertCount($unit->accessories()->count(),$accessories);
        $response = $this->delete("/api/knowledge_unit/$unit->id");
        $response->assertSuccessful();
        $accessories = KuAccessory::query()->where("knowledge_unit_id",$unit->id)->get();
        $this->assertCount(0,$accessories);
        $unit = KnowledgeUnit::query()->find($unit->id);
        $this->assertNull($unit);

    }

    /** @test */
    public function testGetKnowledgeUnitByChapterId(){
        $chapter = factory(BookChapter::class)->create();
        $ku = factory(KnowledgeUnit::class)->create([
            "book_id" => $chapter->book->id,
            "book_chapter_id" => $chapter->id
        ]);
        $response = $this->get("api/knowledge_unit?chapter_id=$chapter->id");
        $response->assertSuccessful();
        $response->assertSee($ku->header);
    }
    /** @test */
    public function testGetKnowledgeUnitByBookId(){
        $book = factory(Book::class)->create();
        $ku = factory(KnowledgeUnit::class)->create([
            "book_id" => $book->id,
            "book_chapter_id" => null
        ]);
        $response = $this->get("api/knowledge_unit?book_id=$book->id");
        $response->assertSuccessful();
        $response->assertSee($ku->header);
    }

    /** @test */
    public function testNextAndPreviousKnowledgeUnit(){
        $ku1 = factory(KnowledgeUnit::class)->create();
        $ku2 = factory(KnowledgeUnit::class)->create([
            "book_id" => $ku1->book_id
        ]);
        $chapter = factory(BookChapter::class)->create([
            "book_id" => $ku1->book_id
        ]);
        $ku3 = factory(KnowledgeUnit::class)->create([
            "book_id" => $ku1->book_id,
            "book_chapter_id" => $chapter->id,
        ]);
        $res = $this->get("api/knowledge_unit/$ku2->id");
        $res->assertJsonPath('data.next_ku.id',$ku3->id);
        $res->assertJsonPath('data.previous_ku.id',$ku1->id);
    }

    /** @test */
    public function testGetExtraDataWithKnowledgeUnit(){
        $ku = factory(KnowledgeUnit::class)->create();
        $c = factory(Category::class)->create();
        $c1 = factory(Category::class)->create();
        $ku->book->categories()->sync([$c->id,$c1->id]);
        $res = $this->get("api/knowledge_unit/$ku->id");
        $result = $res["data"];
        $res->assertSuccessful();
        $res->assertSee($ku->book->name);
        $res->assertSee($ku->book->author->name);
        $res->assertSee($c1->name);
    }
}
