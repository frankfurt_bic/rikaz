<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\BookClass;
use App\Models\Category;
use App\Models\NumberingType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Tests\TestCase;


use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Http\Requests\BookRequest;
use App\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LookUpTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function testBookClass()
    {
        app::setLocale('ar');
        $response = $this->get('api/BookClass');
        $response->assertSuccessful();
    }

    /** @test */
    public function testBookSubClass()
    {
        app::setLocale('ar');
        $bookClass = BookClass::query()->first();
        $response = $this->get('api/BookSubClass/' . $bookClass->id);
        $response->assertSuccessful();
    }

    /** @test */
    public function testNumberingType()
    {
        app::setLocale('ar');

        $response = $this->get('api/NumberingType');
        $response->assertSuccessful();
    }

    /** @test */
    public function testKuAccessoryTypes()
    {
        app::setLocale('ar');

        $response = $this->get('api/KuAccessoriesType');
        $response->assertSuccessful();
    }

}
