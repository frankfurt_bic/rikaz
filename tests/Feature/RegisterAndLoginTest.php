<?php

namespace Tests\Feature;


use App\Models\Book;
use App\Models\BookPublishRequest;
use App\Models\Publisher;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class RegisterAndLoginTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function agentRegisterAndLoginTest(){
        Artisan::call('passport:install');
        $oauth = DB::table('oauth_clients')->where('password_client', 1)->first();
        $agent_data = [
            "email" =>"testagent@test.test",
            "password" => "Qw12345678@",
            "password_confirmation" => "Qw12345678@",
            "first_name"  => "test",
            "last_name" => "agent",
            "client_id" => $oauth->id,
            "client_secret" => $oauth->secret
        ];
        $response = $this->post("api/agent/registerByEmail",$agent_data);
        $response->assertSuccessful();
        $response->assertSee("token");
        $response->assertJsonPath("data.user.email",$agent_data["email"]);
        $response->assertJsonPath("data.user.agent.first_name",$agent_data["first_name"]);
        $response->assertJsonPath("data.user.publisher",null);
        $response->assertJsonPath("data.user.is_admin",false);

        $response2 = $this->post("api/user/loginByEmail",$agent_data,[
            "Accept" => "application/json"
        ]);
        $response2->assertSuccessful();
        $response2->assertSee("token");
        $response2->assertJsonPath("data.user.email",$agent_data["email"]);
        $response2->assertJsonPath("data.user.agent.first_name",$agent_data["first_name"]);
        $response2->assertJsonPath("data.user.is_admin",false);
    }
    /** @test */
    public function publisherRegisterAndLoginTest(){
        Artisan::call('passport:install');
        $oauth = DB::table('oauth_clients')->where('password_client', 1)->first();
        $admin = factory(User::class)->create([
            "is_admin" => true
        ]);
        $this->actingAs($admin,'api');
        $publisher_data = [
            "email" =>"testpublisher@test.test",
            "password" => "Qw@12345678",
            "password_confirmation" => "Qw@12345678",
            "name"  => "test publisher",
            "client_id" => $oauth->id,
            "client_secret" => $oauth->secret
        ];
        $response = $this->post("api/publisher/registerByEmail",$publisher_data,[
            "Accept" => "application/json"
        ]);
        if ($response['success']){
            $response->assertSuccessful();
            $response->assertSee("token");
            $response->assertJsonPath("data.user.email",$publisher_data["email"]);
            $response->assertJsonPath("data.user.publisher.name",$publisher_data["name"]);
            $response->assertJsonPath("data.user.agent",null);
            $response->assertJsonPath("data.user.is_admin",false);
        }
        else{
            $response->assertStatus(411);
        }

        $response2 = $this->post("api/user/loginByEmail",$publisher_data,[
            "Accept" => "application/json"
        ]);
        $response2->assertSuccessful();
        $response2->assertSee("token");
        $response2->assertJsonPath("data.user.email",$publisher_data["email"]);
        $response2->assertJsonPath("data.user.publisher.name",$publisher_data["name"]);
        $response2->assertJsonPath("data.user.agent",null);
        $response2->assertJsonPath("data.user.is_admin",false);

        $not_admin = factory(User::class)->create();
        $this->actingAs($not_admin,'api');
        $publisher_data = [
            "email" =>"testpublisher2@test.test",
            "password" => "Qw@12345678",
            "password_confirmation" => "Qw@12345678",
            "name"  => "test publisher",
            "client_id" => 2,
            "client_secret" => "7hr0EJwIb245hFPFF2RZiq6mvLRfPpGRsrHJHRDN"
        ];
        $response3 = $this->post("api/publisher/registerByEmail",$publisher_data,[
            "Accept" => "application/json"
        ]);
        $response3->assertStatus(403);
        $response3->assertSee('Permission Denied');
    }
    /** @test */
    public function adminRegisterAndLoginTest(){
        Artisan::call('passport:install');
        $oauth = DB::table('oauth_clients')->where('password_client', 1)->first();
        $admin = factory(User::class)->create([
            "is_admin" => true
        ]);
        $this->actingAs($admin,'api');
        $admin_data = [
            "email" =>"testadmin@test.test",
            "password" => "Qw@12345678",
            "password_confirmation" => "Qw@12345678",
            "name"  => "test admin",
            "client_id" => $oauth->id,
            "client_secret" => $oauth->secret
        ];
        $response = $this->post("api/admin/registerByEmail",$admin_data,[
            "Accept" => "application/json"
        ]);
        if ($response['success']){
            $response->assertSuccessful();
            $response->assertSee("token");
            $response->assertJsonPath("data.user.email",$admin_data["email"]);
            $response->assertJsonPath("data.user.name",$admin_data["name"]);
            $response->assertJsonPath("data.user.agent",null);
            $response->assertJsonPath("data.user.publisher",null);
            $response->assertJsonPath("data.user.is_admin",true);
        }
        else{
            $response->assertStatus(411);
        }

        $response2 = $this->post("api/user/loginByEmail",$admin_data,[
            "Accept" => "application/json"
        ]);
        $response2->assertSuccessful();
        $response2->assertSee("token");
        $response2->assertJsonPath("data.user.email",$admin_data["email"]);
        $response2->assertJsonPath("data.user.name",$admin_data["name"]);
        $response2->assertJsonPath("data.user.agent",null);
        $response2->assertJsonPath("data.user.publisher",null);
        $response2->assertJsonPath("data.user.is_admin",true);

        $not_admin = factory(User::class)->create();
        $this->actingAs($not_admin,'api');
        $admin_data = [
            "email" =>"testpublisher@test.test",
            "password" => "Qw@12345678",
            "password_confirmation" => "Qw@12345678",
            "name"  => "test publisher",
            "client_id" => 2,
            "client_secret" => "7hr0EJwIb245hFPFF2RZiq6mvLRfPpGRsrHJHRDN"
        ];
        $response3 = $this->post("api/publisher/registerByEmail",$admin_data,[
            "Accept" => "application/json"
        ]);
        $response3->assertStatus(403);
        $response3->assertSee('Permission Denied');

    }
    /** @test */
    public function userToggleActiveTest(){
        $admin = factory(User::class)->create([
            "is_admin" => true
        ]);
        $this->actingAs($admin,'api');
        $publisher = factory(Publisher::class)->create();
//       success toggle active disable
        $response2 = $this->get('api/user/toggleActive/'.$publisher->user->id);
        $response2->assertSuccessful();
        $response2->assertJsonPath("data.is_active",false);
//       success toggle active enable
        $response3 = $this->get('api/user/toggleActive/'.$publisher->user->id);
        $response3->assertSuccessful();
        $response3->assertJsonPath("data.is_active",true);
//       fail toggle active from user not admin
        $admin2 = factory(User::class)->create([
            "is_admin" => false
        ]);
        $this->actingAs($admin2,'api');
        $response3 = $this->get('api/user/toggleActive/'.$publisher->user->id);
        $response3->assertSee('Permission Denied');

    }
}
