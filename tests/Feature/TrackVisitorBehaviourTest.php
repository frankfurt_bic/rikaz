<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\KnowledgeUnit;
use App\Models\User;
use App\Models\Visit;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\App;
use Laravel\Passport\Passport;
use App\Models\Agent;
use Tests\TestCase;


use App\Http\Controllers\ApiControllers\BaseApiController;
use App\Http\Requests\BookRequest;
use App\Models;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TrackVisitorBehaviourTest extends TestCase
{
  //use DatabaseMigrations;

  public function testTrackBookVisits(){
      Passport::actingAs(factory(Agent::class)->create()->user);
      $book=factory(Book::class)->create();
      $result = $this->json('get', 'api/book/'.$book->id);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $resultTrip =  $resultArray['data'] ;
      self::assertEquals($book->id,$resultTrip['id']);
      sleep(1);
      $this->assertNotNull(Visit::where(['content_type'=>Book::class,'content_id'=>$book->id])->first());
  }
  public function testTrackKnowledgeUnitsVisits(){
      Passport::actingAs(factory(Agent::class)->create()->user);
      $knowledgeUnit=factory(KnowledgeUnit::class)->create();
      $result = $this->json('get', 'api/knowledge_unit/'.$knowledgeUnit->id);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $resultTrip =  $resultArray['data'] ;
      self::assertEquals($knowledgeUnit->id,$resultTrip['id']);
      sleep(1);
      $this->assertNotNull(Visit::where(['content_type'=>KnowledgeUnit::class,'content_id'=>$knowledgeUnit->id])->first());
  }
  public function testTrackAnonymousVisitor(){
      $knowledgeUnit=factory(KnowledgeUnit::class)->create();
      $result = $this->json('get', 'api/knowledge_unit/'.$knowledgeUnit->id);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $resultTrip =  $resultArray['data'] ;
      self::assertEquals($knowledgeUnit->id,$resultTrip['id']);
      sleep(1);
      $this->assertNotNull(Visit::where(['content_type'=>KnowledgeUnit::class,'content_id'=>$knowledgeUnit->id])->first());
  }
  public function testGetBookVisitSummary()
  {
      $book = factory(Book::class)->create();
      #region visits

      $c1 = Models\Country::where(['country_code' => 'c1'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c1']);
      $c2 = Models\Country::where(['country_code' => 'c2'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c2']);
      $c3 = Models\Country::where(['country_code' => 'c3'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c3']);
      $c4 = Models\Country::where(['country_code' => 'c4'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c4']);
      $g1 = Models\Gender::where(['name' => 'male'])->first() ?? factory(Models\Gender::class)->create(['name' => 'male']);
      $g2 = Models\Gender::where(['name' => 'female'])->first() ?? factory(Models\Gender::class)->create(['name' => 'female']);
      $this->json('get', 'api/book/' . $book->id);                                    //anonymous user visit
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c1->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c1->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c2->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c2->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      $this->json('get', 'api/book/' . $book->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/book/' . $book->id);
      #endregion
      sleep(1);//wait for deferred jobs
      $this->assertEquals(10, Visit::where(['content_type' => Book::class, 'content_id' => $book->id])->count());
      $result = $this->json('get', 'api/book/' . $book->id . '/stats',['start_date'=>Carbon::now()->toDateString(),'end_date'=>Carbon::now()->toDateString()]);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $this->assertEqualsWithDelta([
          'countries_rate' =>
              [
                  0 => ['rate' => '10.00',
                      'country_id' => null],
                  1 => ['rate' => '20.00',
                      'country_id' => $c1->id],
                  2 => ['rate' => '20.00',
                      'country_id' => $c2->id],
                  3 => ['rate' => '50.00',
                      'country_id' => $c4->id]
              ],
//          'genders_rate' =>
//              [
//                  0 => ['rate' => 0.11,
//                      'gender_id' => null],
//                  1 => ['rate' => 0.44,
//                      'gender_id' => $g1->id],
//                  2 => ['rate' => 0.44,
//                      'gender_id' => $g2->id],
//              ],
          'visits' => 10,
          'visitors' => 9,
          'registered_users' => 88.89,
          'anonymous_users' =>11.11
          ], $resultArray['data'], 0.01);
  }
  public function testGetKnowledgeUnitVisitSummary()
  {
      $knowledgeUnit = factory(KnowledgeUnit::class)->create();
      #region visits

      $c1 = Models\Country::where(['country_code' => 'c1'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c1']);
      $c2 = Models\Country::where(['country_code' => 'c2'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c2']);
      $c3 = Models\Country::where(['country_code' => 'c3'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c3']);
      $c4 = Models\Country::where(['country_code' => 'c4'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c4']);
      $g1 = Models\Gender::where(['name' => 'male'])->first() ?? factory(Models\Gender::class)->create(['name' => 'male']);
      $g2 = Models\Gender::where(['name' => 'female'])->first() ?? factory(Models\Gender::class)->create(['name' => 'female']);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);                                    //anonymous user visit
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c1->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c1->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c2->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c2->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g2->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      Passport::actingAs(factory(Agent::class)->create(['country_id' => $c4->id, 'gender_id' => $g1->id])->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id);
      #endregion
      sleep(1);//wait for deferred jobs
      $this->assertEquals(10, Visit::where(['content_type' => KnowledgeUnit::class, 'content_id' => $knowledgeUnit->id])->count());
      $result = $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit->id . '/stats',['start_date'=>Carbon::now()->toDateString(),'end_date'=>Carbon::now()->toDateString()]);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $this->assertEqualsWithDelta([
          'countries_rate' =>
              [
                  0 => ['rate' => '10.00',
                      'country_id' => null],
                  1 => ['rate' => '20.00',
                      'country_id' => $c1->id],
                  2 => ['rate' => '20.00',
                      'country_id' => $c2->id],
                  3 => ['rate' => '50.00',
                      'country_id' => $c4->id]
              ],
//          'genders_rate' =>
//              [
//                  0 => ['rate' => 0.11,
//                      'gender_id' => null],
//                  1 => ['rate' => 0.44,
//                      'gender_id' => $g2->id],
//                  2 => ['rate' => 0.44,
//      'gender_id' => $g1->id],
//              ],
          'visits' => 10,
          'visitors' => 9,
          'registered_users' => 88.89,
          'anonymous_users' => 11.11], $resultArray['data'], 0.01);
  }
  public function testGetAgentVisitSummary()
  {
      $agent = factory(Agent::class)->create();

      $class1 = factory(Models\BookClass::class)->create();
      $class2 = factory(Models\BookClass::class)->create();
      $class3 = factory(Models\BookClass::class)->create();
      $subClass1 = factory(Models\BookSubClass::class)->create();
      $subClass2 = factory(Models\BookSubClass::class)->create();
      $subClass3 = factory(Models\BookSubClass::class)->create();
      $book1 = factory(Book::class)->create(['book_class_id' => $class1->id, 'book_sub_class_id' => $subClass1]);
      $book2 = factory(Book::class)->create(['book_class_id' => $class1->id, 'book_sub_class_id' => $subClass1]);
      $book3 = factory(Book::class)->create(['book_class_id' => $class1->id, 'book_sub_class_id' => $subClass1]);
      $book4 = factory(Book::class)->create(['book_class_id' => $class2->id, 'book_sub_class_id' => $subClass2]);
      $book5 = factory(Book::class)->create(['book_class_id' => $class3->id, 'book_sub_class_id' => $subClass3]);
      $book6 = factory(Book::class)->create(['book_class_id' => $class3->id, 'book_sub_class_id' => $subClass2]);
      $knowledgeUnit1 = factory(KnowledgeUnit::class)->create();
      $knowledgeUnit2 = factory(KnowledgeUnit::class)->create();
      $knowledgeUnit3 = factory(KnowledgeUnit::class)->create();
      $knowledgeUnit4 = factory(KnowledgeUnit::class)->create();
      $knowledgeUnit5 = factory(KnowledgeUnit::class)->create();
      $knowledgeUnit6 = factory(KnowledgeUnit::class)->create();
      Passport::actingAs($agent->user);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit1->id);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit2->id);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit3->id);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit4->id);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit5->id);
      $this->json('get', 'api/knowledge_unit/' . $knowledgeUnit5->id);
      $this->json('get', 'api/book/' . $book1->id);
      $this->json('get', 'api/book/' . $book2->id);
      $this->json('get', 'api/book/' . $book3->id);
      $this->json('get', 'api/book/' . $book4->id);
      $this->json('get', 'api/book/' . $book5->id);
      $this->json('get', 'api/book/' . $book5->id);
      $this->assertEquals(12, Visit::where(['visitor_type' => Agent::class, 'visitor_id' => $agent->id])->count());
      $result = $this->json('get', 'api/agent/' . $agent->id . '/stats');
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $this->assertEqualsWithDelta([
          'books_rate' =>
              [
                  0 => ['rate' => '16.67',
                      'content_id' => $book1->id],
                  1 => ['rate' => '16.67',
                      'content_id' => $book2->id],
                  2 => ['rate' => '16.67',
                      'content_id' => $book3->id],
                  3 => ['rate' => '16.67',
                      'content_id' => $book4->id],
                  4 => ['rate' => '33.33',
                      'content_id' => $book5->id],

              ],
          'knowledge_units_rate' =>
              [
                  0 => ['rate' => '16.67',
                      'content_id' => $knowledgeUnit1->id],
                  1 => ['rate' => '16.67',
                      'content_id' => $knowledgeUnit2->id],
                  2 => ['rate' => '16.67',
                      'content_id' => $knowledgeUnit3->id],
                  3 => ['rate' => '16.67',
                      'content_id' => $knowledgeUnit4->id],
                  4 => ['rate' => '33.33',
                      'content_id' => $knowledgeUnit5->id],
              ],
          'book_class_rate' =>
              [
                  0 => ['rate' => '50.00',
                      'book_class_id' => $class1->id],
                  1 => ['rate' =>  '16.67',
                      'book_class_id' => $class2->id],
                  2 => ['rate' => '33.33',
                      'book_class_id' => $class3->id],


              ],
          'book_sub_class_rate' =>
              [
                  0 => ['rate' => '50.00',
                      'book_sub_class_id' => $subClass1->id],
                  1 => ['rate' =>  '16.67',
                      'book_sub_class_id' => $subClass2->id],
                  2 => ['rate' => '33.33',
                      'book_sub_class_id' => $subClass3->id],

              ],
          'all_visits' => 12,
          'all_items' => 10,
          'all_books' => 5,
          'all_books_visits' => 6,
          'all_knowledge_units' => 5,
          'all_knowledge_units_visits' => 6,], $resultArray['data'], 0.01);
  }
  public function testGetYearlyVisitSummary(){
      $book = factory(Book::class)->create();
      #region visits
//      $model->created_at = Carbon::now();
//      $model->save(['timestamps' => false]);
      $c1 = Models\Country::where(['country_code' => 'c1'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c1']);

      $g1 = Models\Gender::where(['name' => 'male'])->first() ?? factory(Models\Gender::class)->create(['name' => 'male']);

      for($i=10;$i<=20;$i++){

          if($i%3==0){
              Carbon::setTestNow('20'. ($i<10?('0'.$i):$i) .'-10-17');
              Passport::actingAs(factory(Agent::class)->create(['country_id' => $c1->id, 'gender_id' => $g1->id])->user);
              $this->json('get', 'api/book/' . $book->id);
              if ($i%2!=0)
              {
                  $this->json('get', 'api/book/' . $book->id);
              }
          }
      }
      #endregion
      sleep(1);//wait for deferred jobs
      $this->assertEquals(4, Visit::where(['content_type' => Book::class, 'content_id' => $book->id])->count());
      $result = $this->json('get', 'api/book/' . $book->id . '/yearly_stats',['start_date'=>'2010-10-17','end_date'=>'2019-10-17']);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $this->assertEqualsWithDelta([
          'years' =>
              [
                  0 => [
                      'visits'=>1,
                      'visit_rate' => '25.00',
                      'year' => 2012,
                      ],
                  1 => [
                      'visits'=>2,
                      'visit_rate' => '50.00',
                      'year' => 2015,
                      ],
                  2 => [
                      'visits'=>1,
                      'visit_rate' => '25.00',
                      'year' => 2018,
                      ],



              ],
          'visits' => 4,
          'visitors' => 3,
          'registered_users' => 100,
          'anonymous_users' => 0
      ], $resultArray['data'], 0.01);
  }
  public function testGetMonthlyVisitSummary(){
      $book = factory(Book::class)->create();
      #region visits
//      $model->created_at = Carbon::now();
//      $model->save(['timestamps' => false]);
      $c1 = Models\Country::where(['country_code' => 'c1'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c1']);

      $g1 = Models\Gender::where(['name' => 'male'])->first() ?? factory(Models\Gender::class)->create(['name' => 'male']);

      for($i=1;$i<=12;$i++){
          Carbon::setTestNow('2019-' . ($i<10?('0'.$i):$i) . '-17');
          Passport::actingAs(factory(User::class)->create());
          $this->json('get', 'api/book/' . $book->id);                                    //anonymous user visit
          if($i%2==0){
              Passport::actingAs(factory(Agent::class)->create(['country_id' => $c1->id, 'gender_id' => $g1->id])->user);
              $this->json('get', 'api/book/' . $book->id);
          }
      }
      #endregion
      sleep(1);//wait for deferred jobs
      $this->assertEquals(18, Visit::where(['content_type' => Book::class, 'content_id' => $book->id])->count());
      $result = $this->json('get', 'api/book/' . $book->id . '/monthly_stats',['start_date'=>'2019-01-17','end_date'=>'2019-12-17']);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $this->assertEqualsWithDelta([
          'months' =>
              [
                  0 => [
                      'visits'=>1,
                      'visit_rate' => '5.56',
                      'month' => 1,
                      'year' => 2019,
                      ],
                  1 => [
                      'visits'=>2,
                      'visit_rate' => '11.11',
                      'month' => 2,
                      'year' => 2019,
                      ],
                  2 => [
                      'visits'=>1,
                      'visit_rate' => '5.56',
                      'month' => 3,
                      'year' => 2019,
                      ],
                  3 => [
                      'visits'=>2,
                      'visit_rate' => '11.11',
                      'month' => 4,
                      'year' => 2019,
                      ],
                  4 => [
                      'visits'=>1,
                      'visit_rate' => '5.56',
                      'month' => 5,
                      'year' => 2019,
                      ],
                  5 => [
                      'visits'=>2,
                      'visit_rate' => '11.11',
                      'month' => 6,
                      'year' => 2019,
                      ],
                  6 => [
                      'visits'=>1,
                      'visit_rate' => '5.56',
                      'month' => 7,
                      'year' => 2019,
                      ],
                  7 => [
                      'visits'=>2,
                      'visit_rate' => '11.11',
                      'month' => 8,
                      'year' => 2019,
                      ],
                  8 => [
                      'visits'=>1,
                      'visit_rate' => '5.56',
                      'month' => 9,
                      'year' => 2019,
                      ],
                  9 => [
                      'visits'=>2,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      ],
                  10 => [
                      'visits'=>1,
                      'visit_rate' => '5.56',
                      'month' => 11,
                      'year' => 2019,
                      ],
                  11 => [
                      'visits'=>2,
                      'visit_rate' => '11.11',
                      'month' => 12,
                      'year' => 2019,
                      ],

              ],
          'visits' => 18,
          'visitors' => 18,
          'registered_users' => 33.33,
          'anonymous_users' => 66.67
      ], $resultArray['data'], 0.01);
  }
  public function testGetDailyVisitSummary(){
      $book = factory(Book::class)->create();
      #region visits
//      $model->created_at = Carbon::now();
//      $model->save(['timestamps' => false]);
      $c1 = Models\Country::where(['country_code' => 'c1'])->first() ?? factory(Models\Country::class)->create(['country_code' => 'c1']);

      $g1 = Models\Gender::where(['name' => 'male'])->first() ?? factory(Models\Gender::class)->create(['name' => 'male']);

      for($i=1;$i<=29;$i++){
          if($i%3==0){
              Carbon::setTestNow('2019-10-' . ($i<10?('0'.$i):$i));
              Passport::actingAs(factory(Agent::class)->create(['country_id' => $c1->id, 'gender_id' => $g1->id])->user);
              $this->json('get', 'api/book/' . $book->id);
          }
      }
      #endregion
      sleep(1);//wait for deferred jobs
      $this->assertEquals(9, Visit::where(['content_type' => Book::class, 'content_id' => $book->id])->count());
      $result = $this->json('get', 'api/book/' . $book->id . '/daily_stats',['start_date'=>'2019-10-01','end_date'=>'2019-10-29']);
      $resultArray = (array)$result->decodeResponseJson();
      $result->assertSuccessful();
      $this->assertEqualsWithDelta([
          'days' =>
              [
                  0 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>3
                      ],
                  1 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>6

                  ],
                  2 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>9

                  ],
                  3 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>12

                  ],
                  4 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>15

                  ],
                  5 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>18

                  ],
                  6 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>21

                  ],
                  7 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>24

                  ],
                  8 => [
                      'visits'=>1,
                      'visit_rate' => '11.11',
                      'month' => 10,
                      'year' => 2019,
                      'day'=>27

                  ],


              ],
          'visits' => 9,
          'visitors' => 9,
          'registered_users' => 100,
          'anonymous_users' => 0
      ], $resultArray['data'], 0.01);
  }
  public function testGetDailyVisitSummaryEmptyReturn(){
        $book = factory(Book::class)->create();
        sleep(1);//wait for deferred jobs
        $this->assertEquals(0, Visit::where(['content_type' => Book::class, 'content_id' => $book->id])->count());
        $result = $this->json('get', 'api/book/' . $book->id . '/daily_stats',['start_date'=>'2019-10-01','end_date'=>'2019-10-29']);
        $resultArray = (array)$result->decodeResponseJson();
        $result->assertSuccessful();
        $this->assertEqualsWithDelta([
            'days' =>[],
            'visits' => 0,
            'visitors' => 0,
            'registered_users' => 0,
            'anonymous_users' => 0
        ], $resultArray['data'], 0.01);
    }
}
